package lt.minvib.spraynavig.spryfunction;

import java.util.List;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

public class ObjPointVector {

	private LatLng sPoint;
	private LatLng fPoint;

	private float bearing;



	public ObjPointVector(LatLng s, LatLng f, float bearing) {

		if(validateLatLng(s) && validateLatLng(f))
		{
			sPoint=s; 
			fPoint=f;
			this.bearing= bearing;
		}
	}


	public ObjPointVector(LatLng s, LatLng f) {
		if(validateLatLng(s) && validateLatLng(f))
		{
			sPoint = s;
			fPoint=f;
			this.bearing= calculateBearing(s,f);
		}
	}






	public static boolean validateLatLng( LatLng any)
	{
		if(any == null){
			Log.e("ObjPointVector.java", "Vector was given a null value of LatLng");
			try {
				throw new NullPointerException();
			}
			catch (NullPointerException e) {
				System.out.println(e);
				return false;
			}
		}
		return true;
	}



	public static float calculateBearing(LatLng s, LatLng f)
	{

		double lat1 = s.latitude;
		double lat2 = f.latitude;
		double lon1 = s.longitude;
		double lon2 = f.longitude;
		double dLon = lon2-lon1;

		double dPhi = Math.log(Math.tan(lat2/2.0+Math.PI/4.0)/Math.tan(lat1/2.0+Math.PI/4.0));
		if (Math.abs(dLon) > Math.PI)
			if (dLon > 0.0)
				dLon = -(2.0 * Math.PI - dLon);
			else
				dLon = (2.0 * Math.PI + dLon);
		double bearing = (Math.toDegrees(Math.atan2(dLon, dPhi)) + 360.0) % 360.0;
		return (float)bearing;
	}

}
