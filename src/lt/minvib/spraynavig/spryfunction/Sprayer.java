package lt.minvib.spraynavig.spryfunction;

import java.util.ArrayList;
import java.util.List;

import lt.minvib.spraynavig.map.Vr;
import android.graphics.Color;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

public class Sprayer {


	private List<LatLng> mids;
	private List<ObjStripe> rights;
	private List<ObjStripe> lefts;

	private List<LatLng> finalls;

	private List<Polygon> poly;
	private PolygonOptions sprayedPolyOptions;

	private NewCoordinateUtilities ncu;
	private CorrdinateUtilities cu;

	private Polyline mid;

	private static int MAX_POINTS_IN_POLY  = 40;

	private int counter;


	public double sprayWidth = 10.0;
	private int tolerancePoints;

	private static double bearingTolerance = 0.001;
	
	private double lastbearing;
	private double bearing;

	private int color = 0xB100FF00;
	private int color2= 0xB10FFF00;


	public Sprayer() {
		ncu = new NewCoordinateUtilities();
		cu = new CorrdinateUtilities();

		tolerancePoints =(int) (sprayWidth* 3.14 /2);

		counter = 0;
	}

	public boolean validatePoint()
	{
		return false;
	}
	
	
	public void deleteSprayed(){
		if(poly != null && poly.size() >0){
			for(int i = 0; i < poly.size(); i++)
			{
				poly.get(i).remove();
			}
		}
		
		if(mid!= null)
		{
			mid.remove();
		}
		
		
	}


	public void sprayPoint(LatLng point )
	{
		TestTimeUtility time = new TestTimeUtility();
		time.start();

		// On start spraying
		if(mids == null || mids.size() <1)
		{
			ifStart(point);
			return;
		}

		// on first normal point is available
		if(mids.size() <2)
		{
			ifAfterStart(point);
			return;
		} 
		

		bearing =  cu.lineSegmentBearing(mids.get(mids.size()-1), point);

		boolean isBearingCool = false;

		if(Math.abs(getDifference(bearing,lastbearing)) < bearingTolerance)
			isBearingCool = true;


		lastbearing =  bearing;

		LatLng pt1 = ncu.linePointByBearingAndDistance(point, bearing, sprayWidth, 90);
		LatLng pt2 = ncu.linePointByBearingAndDistance(point, bearing, sprayWidth,-90);

		if(isBearingCool && rights.size() > 1 && lefts.size()>1)
		{
			
			rights.set(rights.size()-1, new ObjStripe(pt1, point));
			lefts.set(lefts.size()-1, new ObjStripe(pt2, point));
		} else {
			
			

			

			if(!findIntersectionIndx(pt1, point, rights,1)){ 
				
			//	LatLng pt = ncu.linePointByBearingAndDistance(point, lastbearing, sprayWidth, 90);
			//	rights.add(new ObjStripe(pt, point));
				rights.add(new ObjStripe(pt1, point));
			}
			
			else {rights  = removeUslessLines(pt1, point, rights);}

			if(!findIntersectionIndx(pt2, point,lefts,1) ){ 
				
			//	LatLng pt = ncu.linePointByBearingAndDistance(point, lastbearing, sprayWidth, -90);
			//	lefts.add(new ObjStripe(pt, point));
				lefts.add(new ObjStripe(pt2, point)); 
				
			} 
			else {lefts  = removeUslessLines(pt2, point, lefts);}

		}

		mid = PolylineUtils.addPoint (mid, point);
		mids.add(point);

		recalculateFinnal();

		// TODO - Self Intersection implementation
		if(isFinalSelfIntersectsOrhaveTooMuchPoints())
		{
			ObjStripe r1 = rights.get(rights.size()-2); ObjStripe r2 =  rights.get(rights.size()-1);
			ObjStripe l1 =  lefts.get(lefts.size()-2);  ObjStripe l2 =  lefts.get(lefts.size()-1);

			rights = new ArrayList<ObjStripe>();
			lefts = new ArrayList<ObjStripe>();

			setSprayPolygonOptions(r1.getSide(), l1.getSide());
			rights.add(r1);
			lefts.add(l1);
			rights.add(r2);
			lefts.add(l2);
			
			mid.remove();
			mid = Vr.map.addPolyline(new PolylineOptions().add(point).color(Color.BLACK).width(3));
			
			recalculateFinnal();
		} 
		redrawPolygon();

		Log.i("Sprayer.java", time.finish("Spraying one point took"));
	}




	private void ifStart(LatLng pt)
	{
		mids = new ArrayList<LatLng>();
		mids.add(pt);
		poly = new ArrayList<Polygon>();
		finalls = new ArrayList<LatLng>();
	}

	private void ifAfterStart(LatLng point)
	{
		rights = new ArrayList<ObjStripe>();
		lefts = new ArrayList<ObjStripe>();

		double bearing =  cu.lineSegmentBearing(mids.get(mids.size()-1), point);
		LatLng pt1 = ncu.linePointByBearingAndDistance(point, bearing, sprayWidth, 90);
		LatLng pt2 = ncu.linePointByBearingAndDistance(point, bearing, sprayWidth, -90);
		

		mid = Vr.map.addPolyline(new PolylineOptions().add(point).color(Color.BLACK).width(3));
		//Vr.map.addPolyline(new PolylineOptions().add(pt2).add(pt1).color(Color.DKGRAY).width(3));

		setSprayPolygonOptions(pt2,pt1);
		rights.add(new ObjStripe(pt1, mids.get(mids.size()-1)));
		lefts.add(new ObjStripe(pt2, mids.get(mids.size()-1)));

		mids.add(point);
		lastbearing =  bearing;
	}





	/**  Removes lines from end of list that are in intersection with given
	 * 
	 * @param pt - first point of stripe
	 * @param point - second point of stripe
	 * @param pl  - stripes of side
	 * @return new stripe list
	 */
	private List<ObjStripe> removeUslessLines(LatLng pt, LatLng point, List<ObjStripe> pl)
	{
		boolean deleteLines = true;
		int iter = 1;
		while(deleteLines)
		{
			deleteLines = false;
			if(findIntersectionIndx(pt, point, pl,iter) && pl.size() >1)
			{
				pl.remove(pl.size()-iter);
				iter ++;
				deleteLines = true;
			}
		}
		return pl;
	}


	/** finds intersecon of line (pt,min) and ss.get(ss.size()-iter)
	 * 
	 * @param pt    - point
	 * @param mid	- second point
	 * @param ss	- lines list
	 * @param iter  - position number form back
	 * @return - intersects or not
	 */
	private boolean findIntersectionIndx(LatLng pt, LatLng mid, List<ObjStripe> ss, int iter)
	{
		if(ss != null  && ss.size() >= iter){
			LatLng[] sd = {pt,mid};
			LatLng[] nd =  {ss.get(ss.size()-iter).getSide(), ss.get(ss.size()-iter).getMid()};
			return cu.isLinesIntersect( sd,nd);
		} else {
			return false;
		}
	}


	/**  Recalculates final LatLng list of Polygon
	 * 
	 */
	private void recalculateFinnal()
	{
		finalls = new  ArrayList<LatLng>();
		for(int i = 0; i < rights.size(); i++ )
			finalls.add(rights.get(i).getSide());

		for(int i =  lefts.size() -1; i >= 0; i-- )
			finalls.add(lefts.get(i).getSide());

		finalls.add(rights.get(0).getSide());
		finalls.add(lefts.get(0).getSide());
	}


	
	/** validates new values of right and left side of the spraying polygon. 
	 * 
	 * @return - returns do you need to create new polygon
	 */
	private boolean isFinalSelfIntersectsOrhaveTooMuchPoints()
	{
		// if size is less than needed for calculations automatically asumed that there is no need to create new polygon
		if(rights == null || rights.size()< 2  || lefts == null || lefts.size()< 2 ){
			
			return false;
		}
			
		
		// if max size of points is reached
		if(finalls.size() > MAX_POINTS_IN_POLY)
			return true;
		
		

		LatLng [] line1 = {rights.get(rights.size()-2).getSide(), rights.get(rights.size()-1).getSide()  }; // right side line (before last; last)
		LatLng [] line2 = {lefts.get(lefts.size()-2).getSide(), lefts.get(lefts.size()-1).getSide()  };		// left  side line (before last; last)
		LatLng [] line3 = {lefts.get(lefts.size()-1).getSide(), rights.get(rights.size()-1).getSide()  };	// final line between right side and left side (Left_last, Right_last)
		LatLng [] line4 = {lefts.get(lefts.size()-2).getSide(), rights.get(rights.size()-2).getSide()  };	// final line between right side and left side (Left_last-1, Right_last-1)
		
		// something of the hook  TODO
		if( rights.size()== 2  &&  lefts.size() == 2 )
			if(cu.isLinesIntersect(line3, line4))
			{
				ObjStripe tmp = rights.get(rights.size()-2);
				rights.set(rights.size()-2, rights.get(rights.size()-1));
				rights.set(rights.size()-1, tmp);
				recalculateFinnal();
				
				return false;
			}
			
		
		// cycle thru all list of final vertices list
		for(int i = 1; i < finalls.size() ; i++)
		{
			LatLng [] lineCh = {finalls.get(i-1), finalls.get(i)}; // line between vertices

			// if lines intersects returns true
			if(cu.isLinesIntersect(line1, lineCh) &&  !isLinesHaveSamePoints(line1, lineCh))
				return true;
			if(cu.isLinesIntersect(line2, lineCh) &&  !isLinesHaveSamePoints(line2, lineCh))
				return true;
			if(cu.isLinesIntersect(line3, lineCh) &&  !isLinesHaveSamePoints(line3, lineCh))
				return true;
		}
		return false;
	}


	private boolean isLinesHaveSamePoints(LatLng[] latlng1, LatLng[] latlng2){


		if(latlng1[0] == latlng2[0] || latlng1[1] == latlng2[0] ||
				latlng1[1] == latlng2[1] || latlng1[0] == latlng2[1])
			return true;

		return false;
	}




	/** redraws polygon
	 * 
	 */
	private void redrawPolygon()
	{
		Polygon pol = poly.get(poly.size()-1);
		pol.setPoints(finalls);
		poly.set(poly.size()-1, pol);
	}




	private String latLngtoString(LatLng pt)
	{
		return "Latitude: " + String.valueOf(pt.latitude) + " Longtitude: " + String.valueOf(pt.longitude);
	}


	private void setSprayPolygonOptions(LatLng pt1, LatLng pt2)
	{
		sprayedPolyOptions = new PolygonOptions()
		.fillColor(color)
		.strokeColor(Color.RED)
		.strokeWidth(2)
		.geodesic(true)
		.add(pt1).add(pt2);
		poly.add( Vr.map.addPolygon(sprayedPolyOptions));
	}


	private double getDifference(double a1, double a2) {
		return Math.min((a1-a2)<0?a1-a2+360:a1-a2, (a2-a1)<0?a2-a1+360:a2-a1);
	}



	//		Log.i("Sprayer.java", "Fisrst " +latLngtoString(pt) +" "+ latLngtoString(mid)  );
	//		Log.i("Sprayer.java", "Second " +latLngtoString(sss.get(sss.size()-iter)) +" "+ latLngtoString(midd.get(midd.size()-iter))  );

}
