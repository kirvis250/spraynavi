package lt.minvib.spraynavig.spryfunction;

public class TestTimeUtility {

	
	private long start;
	private long finish;
	
	public TestTimeUtility() {}
	
	
	public void start()
	{start = System.currentTimeMillis();}
	
	
	public String finish(String subject)
	{
		finish = System.currentTimeMillis();
		long result = finish - start; 
	
		String res = subject + " :" + String.valueOf(result);
		
		return res;
	}
	
}
