package lt.minvib.spraynavig.spryfunction;

import java.util.List;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;

public class PolylineUtils {
	
	
	/** add point to polyline
	 * 
	 * @param pol - given polyline
	 * @param point  - new point
	 * @return - Changed polyline
	 */
	public static Polyline addPoint( Polyline pol, LatLng point )
	{
		List<LatLng> pts = pol.getPoints();
		pts.add(point);
		pol.setPoints(pts);
		return pol;
	}


	/** set point in polyline
	 * 
	 * @param pol - given polyline
	 * @param point - new point
	 * @param loc - location that will be changed
	 * @return - changed pollyline
	 */
	public static Polyline setPoint( Polyline pol, LatLng point, int loc )
	{
		List<LatLng> pts = pol.getPoints();
		pts.set(loc,point);
		pol.setPoints(pts);
		return pol;
	}



	/** remove point form given Polyline
	 * 
	 * @param pol -given polyline
	 * @param loc -point index 
	 * @return - changed polyline
	 */
	public static Polyline removePoint( Polyline pol, int loc )
	{
		List<LatLng> pts = pol.getPoints();
		pts.remove(loc);
		pol.setPoints(pts);
		return pol;
	}



}
