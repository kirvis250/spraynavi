package lt.minvib.spraynavig.spryfunction;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;


/**
 * 
 * @author Mindaugas Viburys 
 * @version 0.1v
 *
 */
public class NewCoordinateUtilities {
	
	public static final double EARTH_RADIUS = 6371; // km
	
	

	/** Set point by locaction and it's bearing by atstumas in meters
	 * 
	 * @param coords   - starting coordinates
	 * @param bearing  - bearing of the movement
	 * @param atstumas - prefered distance form point
	 * @param degrees  - degree amount of new point by bearing. 
	 * @return	- new point coordinates
	 */
	public LatLng linePointByBearingAndDistance(LatLng coords, double bearing, double atstumas, double degrees)
	{

		double R = 6356800;
		double dist = atstumas;
		double brng =  bearing + degrees;

		dist = dist/R;  // convert dist to angular distance in radians
		brng = brng * Math.PI / 180;  // 
		double lat1 = coords.latitude * Math.PI / 180 ;
		double lon1 = coords.longitude * Math.PI / 180;

		double lat2 = Math.asin( Math.sin(lat1)*Math.cos(dist) + 
				Math.cos(lat1)*Math.sin(dist)*Math.cos(brng) );
		double lon2 = lon1 + Math.atan2(Math.sin(brng)*Math.sin(dist)*Math.cos(lat1), 
				Math.cos(dist)-Math.sin(lat1)*Math.sin(lat2));
		lon2 = (lon2+3*Math.PI) % (2*Math.PI) - Math.PI;  // normalise to -180..+180Āŗ

		return new LatLng( lat2*180/ Math.PI , lon2*180/ Math.PI);

	}
	
	/** Set point by locaction and it's bearing by atstumas in meters
	 * 
	 * @param loc			- android.location.Location of current position 
	 * @param atstumas		- prefered distance form point
	 * @param degrees		- degree amount of new point by location bearing.
	 * @return	- new point coordinates
	 */
	public LatLng linePointByBearingAndDistance(Location loc, double atstumas, double degrees)
	{

		double R = 6356800;
		double dist = atstumas;

		double brng = loc.getBearing() + degrees;

		dist = dist/R;  // convert dist to angular distance in radians
		brng = brng * Math.PI / 180;  // 
		double lat1 = loc.getLatitude() * Math.PI / 180 ;
		double lon1 = loc.getLongitude() * Math.PI / 180;

		double lat2 = Math.asin( Math.sin(lat1)*Math.cos(dist) + 
				Math.cos(lat1)*Math.sin(dist)*Math.cos(brng) );
		double lon2 = lon1 + Math.atan2(Math.sin(brng)*Math.sin(dist)*Math.cos(lat1), 
				Math.cos(dist)-Math.sin(lat1)*Math.sin(lat2));
		lon2 = (lon2+3*Math.PI) % (2*Math.PI) - Math.PI;  // normalise to -180..+180Āŗ

		return new LatLng( lat2*180/ Math.PI , lon2*180/ Math.PI);

	}
	
	
	/** diastance between two points 
	 * 
	 * @param p1 - coordinate one
	 * @param p2 - coordinate two
	 * @return - distance in meters
	 */
	public double distanceBetweenPoints(LatLng p1, LatLng p2)
	{
	
		double lat1 = Math.toRadians(p1.latitude);
		double lat2 =  Math.toRadians(p2.latitude);		
		
		double deltaLat = Math.toRadians(p2.latitude -p1.latitude);	
		
		double deltalon = Math.toRadians(p2.longitude -p1.longitude);
		
		
		double a = Math.sin(deltaLat/2)* Math.sin(deltaLat/2) +
				Math.cos(lat1) * Math.cos(lat2) *
				Math.sin(deltalon/2)* Math.sin(deltalon/2);
		
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		
		return EARTH_RADIUS * c *1000;
	}
	
	

}
