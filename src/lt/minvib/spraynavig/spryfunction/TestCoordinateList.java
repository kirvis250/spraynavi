package lt.minvib.spraynavig.spryfunction;

import java.util.List;

import com.google.android.gms.maps.model.LatLng;

public class TestCoordinateList {
	
	private List<LatLng> list;

	public List<LatLng> getList() {return list;}
	public void setList(List<LatLng> list) {this.list = list;}

}
