package lt.minvib.spraynavig.spryfunction;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineSegment;

public class CorrdinateUtilities {
	

	/** checks if marker is in area of another array of markers
	 * 
	 * @param spoint
	 * @param polygon
	 * @return
	 */
	public boolean isPointInPolygon( Coordinate point, List<LineSegment> polygon)
	{
		int i, j , nvert = polygon.size();
		boolean c = false;

		List<Coordinate> cord = new ArrayList<Coordinate>();
		for(LineSegment aa : polygon)
		{cord.add(aa.p0);}

		cord.add(polygon.get(polygon.size()-1).p1);

		for(i =0, j= nvert -1 ; i<nvert; j = i++ )
		{
			Coordinate pointsJ = cord.get(j);
			Coordinate pointsI = cord.get(i);
			if(((pointsI.y >=  point.y ) != (pointsJ.y  >=  point.y)) &&
					(point.x <= ( pointsJ.x - pointsI.x ) *
					(point.y  - pointsI.y)/ (pointsJ.y - pointsI.y) +   pointsI.x )
					)	

			{c = !c;}
		}
		return c;
	}


	/** check if point in polygon
	 * 
	 * @param poly
	 * @param point
	 * @return
	 */
	public boolean isPointInPolygon( List<LatLng> poly, LatLng point)
	{


		int nvert = poly.size();
		int i, j;
		boolean c = false; 
		for (i = 0, j = nvert-1; i < nvert; j = i++) {
			if ( ((poly.get(i).latitude>point.latitude) != (poly.get(j).latitude>point.latitude)) &&
					(point.longitude < (poly.get(j).longitude-poly.get(i).longitude) * (point.latitude-poly.get(i).latitude) 
							/ (poly.get(j).latitude-poly.get(i).latitude) + poly.get(i).longitude) )
				c = !c;
		}
		return c;
	}

	public boolean isPointInPolygone( List<Polyline> ppoly, LatLng point)
	{
		List<LatLng> poly = new ArrayList<LatLng>();

		for(int i = 0 ; i< ppoly.size(); i++ )
		{
			poly.add(ppoly.get(i).getPoints().get(0));
			poly.add(ppoly.get(i).getPoints().get(1));
		}

		int nvert = poly.size();
		int i, j;
		boolean c = false; 
		for (i = 0, j = nvert-1; i < nvert; j = i++) {
			if ( ((poly.get(i).latitude>point.latitude) != (poly.get(j).latitude>point.latitude)) &&
					(point.longitude < (poly.get(j).longitude-poly.get(i).longitude) * (point.latitude-poly.get(i).latitude) 
							/ (poly.get(j).latitude-poly.get(i).latitude) + poly.get(i).longitude) )
				c = !c;
		}
		return c;
	}


	public boolean isPointInPolygons( List<Polyline> poly, LatLng point1, LatLng point2)
	{

		return true;
	}



	boolean pnpoly(int nvert, float []vertx, float []verty, float testx, float testy)
	{
		int i, j;
		boolean c = false; 
		for (i = 0, j = nvert-1; i < nvert; j = i++) {
			if ( ((verty[i]>testy) != (verty[j]>testy)) &&
					(testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
				c = !c;
		}
		return c;
	}

	/**
	 * 
	 * @param spoint
	 * @param polygon
	 * @return
	 */
	@Deprecated
	public PolylineOptions getClosestLine( Marker spoint, List<Marker> polygon)
	{
		LatLng  testee=  spoint.getPosition(); 						// Testuojamas taÅ?kas

		LatLng poly1 = polygon.get(0).getPosition();				// pirmas tieses taÅ?kas
		LatLng poly2 = polygon.get(1).getPosition();				// antras tieses taÅ?kas

		PolylineOptions returnable = new PolylineOptions()			// formuojami tiesÄ—s nustatymai
		.add(poly1 ,poly2 )
		.width(8)
		.color(Color.RED)
		.geodesic(true);

		double mindist = distanceFormPointToLine(testee, poly1, poly2);	// maÅ¾iausias atstumas iki tieses

		double dist;												// einamasis atstmas

		// ciklas per visas tieses
		for(int i= 2; i< polygon.size(); i++)
		{
			poly1 = polygon.get(i).getPosition();
			poly2 = polygon.get(i-1).getPosition();

			dist = distanceFormPointToLine(testee, poly1, poly2);
			if(dist < mindist)
			{
				mindist = dist;
				returnable = new PolylineOptions()			// formuojami tiesÄ—s nustatymai
				.add(poly1 ,poly2 )
				.width(8)
				.color(Color.RED)
				.geodesic(true);
			}

		}

		return returnable;
	}

	/**
	 * 
	 * @param pt - checkable thing
	 * @param p1 - first line point	
	 * @param p2 - second line point
	 * @return
	 */
	public double distanceFormPointToLine(LatLng pt, LatLng p1, LatLng p2)
	{
		double a = distanceBetweenPoints(p1,p2);
		double b = distanceBetweenPoints(p1,pt);
		double c = distanceBetweenPoints(p2,pt);

		double semiPerimeter =  (a + b + c)/2;
		double area = Math.sqrt(semiPerimeter* (semiPerimeter - a)*(semiPerimeter -b)*(semiPerimeter - c));
		double dist = area * 2 / a;

		return dist;
	}

	// Matuoti  atstumÄ… tarp latLng taÅ?kÅ³
	public double distanceBetweenPoints( LatLng point1, LatLng point2 )
	{
		int R = 6371;
		double dLat = asRadians(point2.latitude - point1.latitude);
		double dLong = asRadians(point2.longitude - point1.longitude);
		double a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(asRadians(point1.latitude)) * Math.cos(asRadians(point2.latitude)) * Math.sin(dLong/2) * Math.sin(dLong/2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double d = R * c;
		return d;
	}

	public double distanceBetweenPoints( Coordinate point1, Coordinate point2 )
	{
		return point1.distance(point2);
	}

	public double distanceBetweenPointsJTS( LatLng point1, LatLng point2 )
	{
		Coordinate pt1 = this.ToMercator(point1);
		Coordinate pt2 = this.ToMercator(point2);
		return pt1.distance(pt2);
	}

	/**
	 * 
	 * @param segement  - list of area LineSegments
	 * @return	- Line Segment of linePart that is in area
	 */
	public List<LineSegment> cropLineSegmentToArea(List<LineSegment> segment, List<LineSegment> area)
	{

		List<LineSegment> returnable = new ArrayList<LineSegment>();

		for(int j = 0; j < segment.size(); j++)
		{
			LineSegment ss = segment.get(j);
			List<Coordinate> intersections = new ArrayList<Coordinate>();
			// checking if there is intersections to area lines
			for(int z = 0; z < area.size(); z++)
			{	
				Coordinate inter = ss.intersection(area.get(z));
				if(inter != null )
				{intersections.add(inter);}	
			}

			// if there is no interactions return null;

			if(!intersections.isEmpty())
				for( int i = 0 ; i < intersections.size() -1; i++)
					for(int g = i; g < intersections.size(); g++)
					{
						Coordinate gg = intersections.get(g);
						Coordinate ii = intersections.get(i);
						if( (gg.x+gg.y) < (ii.x + ii.y))
						{
							Coordinate tmp  = intersections.get(i);
							intersections.set(i, intersections.get(g));
							intersections.set(g, tmp);
						}
					}

			// set intersections to shtrich
			LineSegment candidate = new LineSegment() ;
			if(!intersections.isEmpty())
				for(int i = 0 ;  i< intersections.size()-1; i++)
				{
					if(i % 2 == 0)
					{
						Coordinate in1 = intersections.get(i);
						Coordinate in2 = intersections.get(i+1);
						candidate = new LineSegment(in1, in2);
						returnable.add(candidate);
					}
				}

		}
		return returnable;
	}

	/** returns nearest line segment to the point of arraylist
	 * 
	 * @param pt
	 * @param lines
	 * @return
	 */
	public LineSegment getNearestLine( Coordinate pt, List<LineSegment> lines )
	{

		LineSegment nearest = lines.get(0);
		double dist = lines.get(0).distance(pt);

		// determines the nearest LineSegment
		for(int i = 0; i < lines.size() ; i++)
		{
			double vardist = lines.get(i).distance(pt);
			if( dist > vardist)
			{
				dist = vardist;
				nearest = lines.get(i);
			}
		}
		return nearest;
	}

	/** returns nearest line segment to the point of arraylist
	 * 
	 * @param pt
	 * @param lines
	 * @return
	 */
	public int getNearestLineIndex( Coordinate pt, List<LineSegment> lines )
	{

		int nearest = 0;
		double dist = lines.get(0).distance(pt);

		// determines the nearest LineSegment
		for(int i = 0; i < lines.size() ; i++)
		{
			double vardist = lines.get(i).distance(pt);
			if( dist > vardist)
			{
				dist = vardist;
				nearest = i;
			}
		}
		return nearest;
	}
	
	
	/** returns nearest line segment to the point of arraylist
	 * 
	 * @param pt
	 * @param lines
	 * @return
	 */
	public int getNearestLineIndexGoogle( LatLng pty, List<Polyline> lines )
	{

		int nearest = 0;
		
		Coordinate pt = ToMercator(pty);
		
		LineSegment aa =  new LineSegment(ToMercator(  lines.get(0).getPoints().get(0)), ToMercator( lines.get(0).getPoints().get(1))); 
		
		double dist = aa.distance(pt);

		// determines the nearest LineSegment
		for(int i = 0; i < lines.size() ; i++)
		{
			aa =  new LineSegment(ToMercator(  lines.get(i).getPoints().get(0)), ToMercator( lines.get(i).getPoints().get(1))); 
			double vardist = aa.distance(pt);
			if( dist > vardist)
			{
				dist = vardist;
				nearest = i;
			}
		}
		return nearest;
	}

	/** returns nearest line segment to the point of arraylist
	 * 
	 * @param pt
	 * @param lines
	 * @return
	 */
	public LineSegment getOutermostLine( Coordinate pt, List<LineSegment> lines )
	{

		LineSegment nearest = lines.get(0);
		double dist = lines.get(0).distance(pt);

		// determines the nearest LineSegment
		for(int i = 0; i > lines.size() ; i++)
		{
			double vardist = lines.get(i).distance(pt);
			if( dist < vardist)
			{
				dist = vardist;
				nearest = lines.get(i);
			}
		}
		return nearest;
	}

	/**
	 * 
	 * @param p   - line wich will be as base line for all lines
	 * @param meters - distance between two lines
	 */
	public List<LineSegment> setShtirch( LineSegment p , double meters, List<LineSegment> lineList, int method ) 
	{

		double lenghtofP =  p.getLength();				// pagrindinÄ—s linijos ilgis
		double [] bounds = getBoundingBox( lineList );  // Poligono ribos

		// nustatymas kiek reikia prailgint linijas
		Coordinate b1,b2;
		b1 = new Coordinate(bounds[0], bounds[1]);	
		b2 = new Coordinate(bounds[2], bounds[3]);	
		double prolongConst = b1.distance(b2);  //pats kintamasis

		Log.i("ShtrichingLegnght: ", String.valueOf(prolongConst) ); // logas

		// logika, prailginimui
		if(lenghtofP < prolongConst)
			p = prolongLineSegment(p,  prolongConst/lenghtofP);
		if(lenghtofP > prolongConst)
			p = prolongLineSegment(p,  prolongConst/lenghtofP *2);

		//LineSegment Z1 = p.
		double x1 = p.p0.x ;
		double x2 = p.p1.x ;
		double y1 = p.p0.y ;
		double y2 = p.p1.y ;

		// linijÅ³ listas
		List <LineSegment> shtrich = new ArrayList<LineSegment>();

		//bazinÄ—s lijijos pridÄ—jimas
		if(method !=1)
			shtrich.add(new LineSegment(p));

		double meters1 = meters;
		double offsett = meters;
		double L = Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));

		if(method == 1)
		{
			double x1p = x1 + meters1 * (y2-y1)/L;
			double x2p = x2 + meters1 * (y2-y1)/L;
			double y1p = y1 + meters1 * (x1-x2)/L;
			double y2p = y2 + meters1 * (x1-x2)/L;

			meters1 = meters1/2 + offsett;

			Coordinate a1 = new Coordinate(x1p, y1p);
			Coordinate a2 = new Coordinate(x2p, y2p);

			shtrich.add(new LineSegment(a1,a2));
		}


		int endofCycle = 0;

		// paiÅ?ymo ÄÆ vienÄ… pusÄ™ ciklas
		while(endofCycle<5)
		{
			double x1p = x1 + meters1*2 * (y2-y1)/L;
			double x2p = x2 + meters1*2 * (y2-y1)/L;
			double y1p = y1 + meters1*2 * (x1-x2)/L;
			double y2p = y2 + meters1*2 * (x1-x2)/L;

			meters1 = meters1 + offsett;

			// [maxX, maxY, minX, minY]
			if( (x1p > bounds[0] && x2p > bounds[0]) ||
					(y1p > bounds[1] && y2p > bounds[1]) ||
					(x1p < bounds[2] && x2p < bounds[2]) ||
					(y1p < bounds[3] && y2p < bounds[3]))
				endofCycle++; 

			Coordinate a1 = new Coordinate(x1p, y1p);
			Coordinate a2 = new Coordinate(x2p, y2p);

			shtrich.add(new LineSegment(a1,a2));
		}
		offsett = meters;
		meters1 = meters;


		if(method == 1)
		{
			double x1p = x1 + meters1 * (y1-y2)/L;
			double x2p = x2 + meters1 * (y1-y2)/L;
			double y1p = y1 + meters1 * (x2-x1)/L;
			double y2p = y2 + meters1 * (x2-x1)/L;

			meters1 = meters1/2 + offsett;

			Coordinate a1 = new Coordinate(x1p, y1p);
			Coordinate a2 = new Coordinate(x2p, y2p);

			shtrich.add(new LineSegment(a1,a2));

		}



		endofCycle = 0; // skaitiklis kiek linijÅ³ iÅ?eina uÅ¾ ribÅ³

		//paiÅ?ymo ÄÆ kitÄ… puse ciklas 
		while(endofCycle<5)
		{
			double x1p = x1 + meters1*2 * (y1-y2)/L;
			double x2p = x2 + meters1*2 * (y1-y2)/L;
			double y1p = y1 + meters1*2 * (x2-x1)/L;
			double y2p = y2 + meters1*2 * (x2-x1)/L;

			meters1 = meters1 + offsett;

			// jei iÅ?eina linija uÅ¾ ribÅ³
			if( (x1p > bounds[0] && x2p > bounds[0]) ||
					(y1p > bounds[1] && y2p > bounds[1]) ||
					(x1p < bounds[2] && x2p < bounds[2]) ||
					(y1p < bounds[3] && y2p < bounds[3]))
				endofCycle++; 

			Coordinate a1 = new Coordinate(x1p, y1p);
			Coordinate a2 = new Coordinate(x2p, y2p);

			shtrich.add(new LineSegment(a1,a2));
		}
		shtrich = cropLineSegmentToArea(shtrich, lineList);
		return shtrich;
	}

	// Prailgina linijas, kad visÄ… laukÄ… tikrai padengtÅ³
	public LineSegment prolongLineSegment(LineSegment p, double lenght )
	{

		if( (p.p1.x + p.p1.y) > (p.p0.x + p.p0.y))
		{
			p.p1.x = p.p1.x + (p.p1.x - p.p0.x) * lenght;
			p.p1.y = p.p1.y + (p.p1.y - p.p0.y) * lenght;

			p.p0.x = p.p1.x - (p.p1.x - p.p0.x) * lenght;
			p.p0.y = p.p1.y - (p.p1.y - p.p0.y) * lenght;

		} else {
			p.p1.x = p.p1.x - (p.p0.x - p.p1.x) * lenght;
			p.p1.y = p.p1.y - (p.p0.y - p.p1.y) * lenght;

			p.p0.x = p.p1.x + (p.p0.x - p.p1.x) * lenght;
			p.p0.y = p.p1.y + (p.p0.y - p.p1.y) * lenght;
		}

		Log.e("ShtrichingLegnght: ", String.valueOf(p.getLength()) );
		return p;
	}

	/** gets perimeter of area
	 * 
	 * @param given
	 * @return
	 */
	public double areaPerimeter (List<LineSegment>  given)
	{
		double perimeter = 0;

		for (int i= 0 ; i< given.size(); i++)
		{perimeter += given.get(i).getLength();}
		return perimeter;
	}

	/** Gets bounding box of an polygon
	 * 
	 * @param given - List of linesegments List<LineSegment>
	 * @return - array [maxX, maxY, minX, minY]
	 */
	public double[] getBoundingBox( List<LineSegment>  given )
	{
		double maxX = given.get(0).p0.x;
		double maxY = given.get(0).p0.y;
		double minX = given.get(0).p0.x;
		double minY = given.get(0).p0.y;

		for(int i = 0; i < given.size(); i++ )
		{
			LineSegment aa = given.get(i);

			if(maxX < aa.p0.x)
				maxX =  aa.p0.x;
			if(maxY < aa.p0.y)
				maxY = aa.p0.y;
			if(minX > aa.p0.x)
				minX = aa.p0.x;
			if(minY > aa.p0.y)
				minY = aa.p0.y;
			if(maxX < aa.p1.x)
				maxX =  aa.p1.x;
			if(maxY < aa.p1.y)
				maxY = aa.p1.y;
			if(minX > aa.p0.x)
				minX = aa.p0.x;
			if(minY > aa.p0.y)
				minY = aa.p0.y;
		}
		double [] returnable  =  {maxX, maxY, minX, minY }; 

		return returnable;
	}

	//*****************************************************************
	//   Line Bearing  
	//*****************************************************************

	// LineSegment bearing
	public double lineSegmentBearing(LineSegment a  )
	{

		double lat1 = Math.toRadians(this.ToLonLat(a.p0).latitude ); 
		double lat2 = Math.toRadians(this.ToLonLat(a.p1).latitude ); 
		double lon1 = Math.toRadians(this.ToLonLat(a.p0).longitude); 
		double lon2 = Math.toRadians(this.ToLonLat(a.p1).longitude);

		double dLon = lon2-lon1;

		double dPhi = Math.log(Math.tan(lat2/2.0+Math.PI/4.0)/Math.tan(lat1/2.0+Math.PI/4.0));
		if (Math.abs(dLon) > Math.PI)
			if (dLon > 0.0)
				dLon = -(2.0 * Math.PI - dLon);
			else
				dLon = (2.0 * Math.PI + dLon);

		double bearing = (Math.toDegrees(Math.atan2(dLon, dPhi)) + 360.0) % 360.0;

		return bearing;
	}

	// Polyline bearing
	public double lineSegmentBearing(Polyline a)
	{
		List<LatLng> points= a.getPoints();

		double lat1 = Math.toRadians( points.get(0).latitude ); 
		double lat2 = Math.toRadians( points.get(1).latitude ); 
		double lon1 = Math.toRadians( points.get(0).longitude); 
		double lon2 = Math.toRadians( points.get(1).longitude);

		double dLon = lon2-lon1;



		double dPhi = Math.log(Math.tan(lat2/2.0+Math.PI/4.0)/Math.tan(lat1/2.0+Math.PI/4.0));
		if (Math.abs(dLon) > Math.PI)
			if (dLon > 0.0)
				dLon = -(2.0 * Math.PI - dLon);
			else
				dLon = (2.0 * Math.PI + dLon);
		double bearing = (Math.toDegrees(Math.atan2(dLon, dPhi)) + 360.0) % 360.0;
		return bearing;
	}

	// Bearing by LatLng points
	public double lineSegmentBearing(LatLng pt1, LatLng pt2)
	{
		double lat1 = Math.toRadians(pt1.latitude ); 
		double lat2 = Math.toRadians(pt2.latitude ); 
		double lon1 = Math.toRadians(pt1.longitude); 
		double lon2 = Math.toRadians(pt2.longitude);

		double dLon = lon2-lon1;

		double dPhi = Math.log(Math.tan(lat2/2.0+Math.PI/4.0)/Math.tan(lat1/2.0+Math.PI/4.0));
		if (Math.abs(dLon) > Math.PI)
			if (dLon > 0.0)
				dLon = -(2.0 * Math.PI - dLon);
			else
				dLon = (2.0 * Math.PI + dLon);

		double bearing = (Math.toDegrees(Math.atan2(dLon, dPhi)) + 360.0) % 360.0;

		return bearing;
	}
	//***********************************************************************


	//********************************************************************************
	// Set point by locaction and it's bearing by atstumas in meters
	//********************************************************************************
	public LatLng linePointByBearingAndDistance(Location loc, double atstumas, double degrees)
	{

		double R = 6356800;
		double dist = atstumas;

		double brng = loc.getBearing() + degrees;

		dist = dist/R;  // convert dist to angular distance in radians
		brng = brng * Math.PI / 180;  // 
		double lat1 = loc.getLatitude() * Math.PI / 180 ;
		double lon1 = loc.getLongitude() * Math.PI / 180;

		double lat2 = Math.asin( Math.sin(lat1)*Math.cos(dist) + 
				Math.cos(lat1)*Math.sin(dist)*Math.cos(brng) );
		double lon2 = lon1 + Math.atan2(Math.sin(brng)*Math.sin(dist)*Math.cos(lat1), 
				Math.cos(dist)-Math.sin(lat1)*Math.sin(lat2));
		lon2 = (lon2+3*Math.PI) % (2*Math.PI) - Math.PI;  // normalise to -180..+180Āŗ

		return new LatLng( lat2*180/ Math.PI , lon2*180/ Math.PI);

	}


	@Deprecated
	public List<LineSegment> ConvateHullAprox(int k, List<Coordinate> coord)
	{
		// TODO

		return null;
	}


	//********************************************************************************
	//  			Check if given lines intersect
	//********************************************************************************
	public boolean isLinesIntersect(Polyline p1 , Polyline p2)
	{
		Coordinate [] coord = {null,null,null,null};
		coord[0] = ToMercator(p1.getPoints().get(0));
		coord[1] = ToMercator(p1.getPoints().get(1));
		coord[3] = ToMercator(p2.getPoints().get(0));
		coord[4] = ToMercator(p2.getPoints().get(1));

		LineSegment lineSeg1 =  new  LineSegment(coord[0], coord[1]);
		LineSegment lineSeg2 =  new  LineSegment(coord[2], coord[3]);

		return lineSeg1.intersection(lineSeg2) != null;
	}

	// do lines intersect
	public boolean isLinesIntersect(LatLng[] latlng1,LatLng[] latlng2 )
	{
		Coordinate [] coord = {null,null,null,null};
		coord[0] = ToMercator(latlng1[0]);
		coord[1] = ToMercator(latlng1[1]);
		coord[2] = ToMercator(latlng2[0]);
		coord[3] = ToMercator(latlng2[1]);

		LineSegment lineSeg1 =  new  LineSegment(coord[0], coord[1]);
		LineSegment lineSeg2 =  new  LineSegment(coord[2], coord[3]);

		return lineSeg1.intersection(lineSeg2) != null;
	}


	//********************************************************************************
	//  			Conversions between LatLng and Cordinate
	//********************************************************************************

	/** From latlng To WGS84 
	 * 
	 * @param lon
	 * @param lat
	 * @return
	 */
	public double[] ToMercator(double lon, double lat)
	{
		double x = lon * 20037508.34 / 180;
		double y = Math.log(Math.tan((90 + lat) * Math.PI / 360)) / (Math.PI / 180);
		y = y * 20037508.34 / 180;
		return new double[] {x, y};
	}

	// From WGS84 to latlng
	/**
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public double[] ToLonLat(double x, double y) 
	{
		double lon = (x / 20037508.34) * 180;
		double lat = (y / 20037508.34) * 180;

		lat = 180/Math.PI * (2 * Math.atan(Math.exp(lat * Math.PI / 180)) - Math.PI / 2);
		return new double[] {lon, lat};
	}

	// From latlng To WGS84 
	/**
	 * 
	 * @param given
	 * @return
	 */
	public Coordinate ToMercator(LatLng  given)
	{
		double lon = given.longitude;
		double lat = given.latitude;
		double x = lon * 20037508.34 / 180;
		double y = Math.log(Math.tan((90 + lat) * Math.PI / 360)) / (Math.PI / 180);
		y = y * 20037508.34 / 180;
		return new Coordinate(x,y);
	}

	// From WGS84 to latlng
	/**
	 * 
	 * @param given
	 * @return
	 */
	public LatLng ToLonLat(Coordinate given) 
	{
		double x =  given.x;
		double y =  given.y;
		double lon = (x / 20037508.34) * 180;
		double lat = (y / 20037508.34) * 180;
		lat = 180/Math.PI * (2 * Math.atan(Math.exp(lat * Math.PI / 180)) - Math.PI / 2);
		return new LatLng(lat, lon);
	}

	//*********************************************************************************
	// 							Old convertions
	//********************************************************************************

	/** degrees as radians
	 * 
	 * @param degrees - given degrees	
	 * @return	 - returnable radians
	 */
	public double asRadians(double degrees)
	{
		return degrees * Math.PI / 180;
	}

	/** converts meters to delta latitude
	 * 
	 * @param lat
	 * @return
	 */
	public double deltaMetersToLatitude( double meters) 
	{
		double deltalat = meters * 360 / 40008000 ; 
		return deltalat;

	}

	/** converts meters to delta logtitude
	 * 
	 * @param meters
	 * @return
	 */
	public double deltaMetersToLongtitude(double meters, double latitude)
	{
		double latitudeCircumference = 40075160 * Math.cos(asRadians(latitude));
		double deltalongtitude = meters * 360 / latitudeCircumference;
		return deltalongtitude;
	}

	/** converts longtitude by meters and latittude
	 * 
	 * @param lng
	 * @param latitude
	 * @return
	 */
	public double LongtitudeToMeters(double lng, double latitude)
	{
		double latitudeCircumference = 40075160 * Math.cos(asRadians(latitude));
		double meters = lng * latitudeCircumference / 360;
		return meters;
	}


	/**
	 * 
	 * @param p   - line wich will be as base line for all lines
	 * @param meters - distance between two lines
	 */
	public List<LineSegment> setShtirch2( LineSegment p , double meters, List<LineSegment> lineList, int method ) 
	{

		double lenghtofP =  p.getLength();				// pagrindinÄ—s linijos ilgis
		double [] bounds = getBoundingBox( lineList );  // Poligono ribos

		// nustatymas kiek reikia prailgint linijas
		Coordinate b1,b2;
		b1 = new Coordinate(bounds[0], bounds[1]);	
		b2 = new Coordinate(bounds[2], bounds[3]);	
		double prolongConst = b1.distance(b2);  //pats kintamasis

		Log.i("ShtrichingLegnght: ", String.valueOf(prolongConst) ); // logas

		// logika, prailginimui
		if(lenghtofP < prolongConst)
			p = prolongLineSegment(p,  prolongConst/lenghtofP);
		if(lenghtofP > prolongConst)
			p = prolongLineSegment(p,  prolongConst/lenghtofP *2);

		// linijÅ³ listas
		List <LineSegment> shtrich = new ArrayList<LineSegment>();

		//bazinÄ—s lijijos pridÄ—jimas
		if(method !=1)
			shtrich.add(new LineSegment(p));

		double meters1 = meters;
		double offsett = meters;

		if(method == 1)
		{
			meters1 =  meters1 - meters*1/2;
			Coordinate a1 = ToMercator( linePointByBearingAndDistance( ToLonLat(p.p0), lineSegmentBearing(p), meters1, 90));
			Coordinate a2 = ToMercator( linePointByBearingAndDistance( ToLonLat(p.p1), lineSegmentBearing(p), meters1, 90));
			meters1 = meters*1/2 + offsett;
			shtrich.add(new LineSegment(a1,a2));
		}

		int endofCycle = 0;

		// paiÅ?ymo ÄÆ vienÄ… pusÄ™ ciklas
		while(endofCycle<5)
		{
			Coordinate a1 = ToMercator( linePointByBearingAndDistance( ToLonLat(p.p0), lineSegmentBearing(p), meters1, 90));
			Coordinate a2 = ToMercator( linePointByBearingAndDistance( ToLonLat(p.p1), lineSegmentBearing(p), meters1, 90));
			meters1 = meters1 + offsett;

			// [maxX, maxY, minX, minY]
			if( (a1.x > bounds[0] && a2.x > bounds[0]) ||
					(a1.y > bounds[1] && a2.y > bounds[1]) ||
					(a1.x < bounds[2] && a2.x < bounds[2]) ||
					(a1.y < bounds[3] && a2.y < bounds[3]))
				endofCycle++; 
			shtrich.add(new LineSegment(a1,a2));
		}
		offsett = meters;
		meters1 = meters;
		if(method == 1)
		{
			meters1 =  meters1 - meters*1/2;
			Coordinate a1 = ToMercator( linePointByBearingAndDistance( ToLonLat(p.p0), lineSegmentBearing(p), meters1, -90));
			Coordinate a2 = ToMercator( linePointByBearingAndDistance( ToLonLat(p.p1), lineSegmentBearing(p), meters1, -90));

			meters1 = meters*1/2 + offsett;
			shtrich.add(new LineSegment(a1,a2));

		}

		endofCycle = 0; // skaitiklis kiek linijÅ³ iÅ?eina uÅ¾ ribÅ³

		//paiÅ?ymo ÄÆ kitÄ… puse ciklas 
		while(endofCycle<5)	
		{
			Coordinate a1 = ToMercator( linePointByBearingAndDistance( ToLonLat(p.p0), lineSegmentBearing(p), meters1, -90));
			Coordinate a2 = ToMercator( linePointByBearingAndDistance( ToLonLat(p.p1), lineSegmentBearing(p), meters1, -90));
			meters1 = meters1 + offsett;

			// jei iÅ?eina linija uÅ¾ ribÅ³
			if( (a1.x > bounds[0] && a2.x > bounds[0]) ||
					(a1.y > bounds[1] && a2.y > bounds[1]) ||
					(a1.x < bounds[2] && a2.x < bounds[2]) ||
					(a1.y < bounds[3] && a2.y < bounds[3]))
				endofCycle++; 

			shtrich.add(new LineSegment(a1,a2));
		}
		shtrich = cropLineSegmentToArea(shtrich, lineList);
		return shtrich;
	}


	//********************************************************************************
	// Set point by locaction and it's bearing by atstumas in meters
	//********************************************************************************
	public LatLng linePointByBearingAndDistance(LatLng coords, double bearing, double atstumas, double degrees)
	{

		double R = 6356800;
		double dist = atstumas;

		double brng =  bearing + degrees;

		dist = dist/R;  // convert dist to angular distance in radians
		brng = brng * Math.PI / 180;  // 
		double lat1 = coords.latitude * Math.PI / 180 ;
		double lon1 = coords.longitude * Math.PI / 180;

		double lat2 = Math.asin( Math.sin(lat1)*Math.cos(dist) + 
				Math.cos(lat1)*Math.sin(dist)*Math.cos(brng) );
		double lon2 = lon1 + Math.atan2(Math.sin(brng)*Math.sin(dist)*Math.cos(lat1), 
				Math.cos(dist)-Math.sin(lat1)*Math.sin(lat2));
		lon2 = (lon2+3*Math.PI) % (2*Math.PI) - Math.PI;  // normalise to -180..+180Āŗ

		return new LatLng( lat2*180/ Math.PI , lon2*180/ Math.PI);

	}
	
	public PolylineOptions lineBybearingOptDistance(LatLng loc, double bearing, double distance)
	{
		LatLng a = linePointByBearingAndDistance(loc, bearing, distance, 0);
		LatLng b = loc;
		
		PolylineOptions opts = new PolylineOptions().add(a).add(b).width(2).color(Color.YELLOW);
		return opts;
	}
	
	


}
