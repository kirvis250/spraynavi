package lt.minvib.spraynavig.spryfunction;

import com.google.android.gms.maps.model.LatLng;

public class ObjStripe {
	
	private LatLng side;
	private LatLng mid;
	
	
	
	public ObjStripe(LatLng side, LatLng mid  ) {
		this.mid  = mid;
		this.side = side;
	}
	
	
	public LatLng getSide() {return side;}
	public void setSide(LatLng side) {this.side = side;}
	public LatLng getMid() {return mid;}
	public void setMid(LatLng mid) {this.mid = mid;}
	
}
