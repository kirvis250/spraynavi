package lt.minvib.spraynavig.spryfunction;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

public class TestGPSclass {


	public interface TestLocationChange{
		public void OnLocationChanged(LatLng locNew);
	}

	List<LatLng> sarasas;

	int renewRate = 4 ;  //Times per second

	int cyclesCount = 1;
	double offset = 0.00;
	

	public void Start(final TestLocationChange callback)
	{
		Thread thread =  new Thread(){

			@Override
			public void run() {

				for(int j = 0; j <cyclesCount ; j++ ){

					for(int i = 0; i < sarasas.size(); i++){

						try {
							Thread.sleep(1000/renewRate);
						} catch (InterruptedException e) {e.printStackTrace();}

						if(callback!= null){
							LatLng  ds = sarasas.get(i);
							callback.OnLocationChanged( new LatLng(ds.latitude+offset, ds.longitude));
						}
					}
					
					offset = offset + 0.00030;

					for(int i = sarasas.size()-1; i > 0; i--){

						try {
							Thread.sleep(1000/renewRate);
						} catch (InterruptedException e) {e.printStackTrace();}

						if(callback!= null){
							LatLng  lat = sarasas.get(i);
							callback.OnLocationChanged( new LatLng(lat.latitude+offset, lat.longitude));
						}
					}
					offset = offset + 0.00030;
				}
			}
		};
		thread.run();
	}



	public void Stop()
	{

	}


	public void  Reset()
	{
		sarasas = spperiorsetData();
	}




	public void save(Object obj, Context ctx) throws IOException
	{
		FileOutputStream fos = ctx.openFileOutput("GPS_DATA", Context.MODE_PRIVATE);
		ObjectOutputStream os = new ObjectOutputStream(fos);
		os.writeObject(obj);
		os.close();
	}

	public TestCoordinateList load(Context ctx) throws StreamCorruptedException, IOException, ClassNotFoundException
	{
		FileInputStream fis = ctx.openFileInput("GPS_DATA");
		ObjectInputStream is = new ObjectInputStream(fis);
		TestCoordinateList simpleClass = (TestCoordinateList) is.readObject();
		is.close();
		return simpleClass;
	}


	public List<LatLng> spperiorsetData()
	{
		List<LatLng> list = new ArrayList<LatLng>();

		list.add(new LatLng(54.9203736 ,23.9495692)); 	 
		list.add(new LatLng(54.9203528 ,23.9496122)); 	 
		list.add(new LatLng(54.9203464 ,23.9496409)); 
		list.add(new LatLng(54.9203381 ,23.9496705));
		list.add(new LatLng(54.9203339 ,23.9496941));
		list.add(new LatLng(54.9203294 ,23.9497169));
		list.add(new LatLng(54.9203235 ,23.9497408));
		list.add(new LatLng(54.9203159 ,23.9497678));
		list.add(new LatLng(54.9203083 ,23.9497925));
		list.add(new LatLng(54.9203038 ,23.9498156));
		list.add(new LatLng(54.9203003 ,23.9498366));
		list.add(new LatLng(54.9202964 ,23.949858 ));
		list.add(new LatLng(54.9202947 ,23.9498776));
		list.add(new LatLng(54.9202982 ,23.9498999));
		list.add(new LatLng(54.9203058 ,23.9499196));
		list.add(new LatLng(54.9203156 ,23.9499364));
		list.add(new LatLng(54.920324 ,23.9499538 ));
		list.add(new LatLng(54.9203279 ,23.9499763));
		list.add(new LatLng(54.9203242 ,23.9499999));
		list.add(new LatLng(54.9203159 ,23.950022 ));
		list.add(new LatLng(54.9203063 ,23.9500414));
		list.add(new LatLng(54.920297 ,23.9500611 ));
		list.add(new LatLng(54.9202904 ,23.950083 ));
		list.add(new LatLng(54.9202858 ,23.9501079));
		list.add(new LatLng(54.9202843 ,23.950133 ));
		list.add(new LatLng(54.9202852 ,23.9501582));
		list.add(new LatLng(54.9202897 ,23.9501808));
		list.add(new LatLng(54.9202974 ,23.9502012));
		list.add(new LatLng(54.9203061 ,23.9502186));
		list.add(new LatLng(54.9203162 ,23.9502341));
		list.add(new LatLng(54.9203255 ,23.9502498));
		list.add(new LatLng(54.9203341 ,23.9502698));
		list.add(new LatLng(54.920339 ,23.9502923 ));
		list.add(new LatLng(54.9203396 ,23.9503166));
		list.add(new LatLng(54.920336 ,23.9503399 ));
		list.add(new LatLng(54.9203299 ,23.9503635));
		list.add(new LatLng(54.9203232 ,23.9503848));
		list.add(new LatLng(54.9203171 ,23.9504058));
		list.add(new LatLng(54.9203139 ,23.9504275));
		list.add(new LatLng(54.9203118 ,23.9504508));
		list.add(new LatLng(54.920308 ,23.9504737 ));
		list.add(new LatLng(54.9203016 ,23.9504953));
		list.add(new LatLng(54.9202939 ,23.9505146));
		list.add(new LatLng(54.920285 ,23.9505333 ));
		list.add(new LatLng(54.9202749 ,23.9505495));
		list.add(new LatLng(54.9202635 ,23.9505623));
		list.add(new LatLng(54.9202516 ,23.9505727));
		list.add(new LatLng(54.9202395 ,23.9505843));
		list.add(new LatLng(54.9202275 ,23.9505944));
		list.add(new LatLng(54.9202159 ,23.9506058));
		list.add(new LatLng(54.9202053 ,23.9506185));
		list.add(new LatLng(54.9201941 ,23.9506334));
		list.add(new LatLng(54.9201826 ,23.9506479));
		list.add(new LatLng(54.9201697 ,23.9506585));
		list.add(new LatLng(54.9201571 ,23.9506661));
		list.add(new LatLng(54.9201445 ,23.9506768));
		list.add(new LatLng(54.9201348 ,23.9506929));
		list.add(new LatLng(54.9201268 ,23.9507117));
		list.add(new LatLng(54.92012 ,23.9507311  ));
		list.add(new LatLng(54.9201137 ,23.9507522));
		list.add(new LatLng(54.9201076 ,23.950773 ));
		list.add(new LatLng(54.9200977 ,23.9507893));
		list.add(new LatLng(54.9200849 ,23.9507959));
		list.add(new LatLng(54.9200706 ,23.9507909));
		list.add(new LatLng(54.9200564 ,23.950781 ));
		list.add(new LatLng(54.9200422 ,23.9507773));
		list.add(new LatLng(54.9200312 ,23.9507871));
		list.add(new LatLng(54.9200253 ,23.9508085));
		list.add(new LatLng(54.9200234 ,23.9508319));
		list.add(new LatLng(54.9200227 ,23.9508553));
		list.add(new LatLng(54.9200207 ,23.9508775));
		list.add(new LatLng(54.9200167 ,23.9508994));
		list.add(new LatLng(54.920009 ,23.9509178 ));
		list.add(new LatLng(54.9199989 ,23.9509321));
		list.add(new LatLng(54.9199877 ,23.9509429));
		list.add(new LatLng(54.9199751 ,23.9509525));
		list.add(new LatLng(54.9199628 ,23.9509613));
		list.add(new LatLng(54.9199506 ,23.9509693));
		list.add(new LatLng(54.9199393 ,23.9509778));
		list.add(new LatLng(54.9199277 ,23.9509889));
		list.add(new LatLng(54.9199175 ,23.9510018));
		list.add(new LatLng(54.919908 ,23.9510158 ));
		list.add(new LatLng(54.9198998 ,23.9510311));
		list.add(new LatLng(54.9198901 ,23.9510466));
		list.add(new LatLng(54.9198789 ,23.9510573));
		list.add(new LatLng(54.9198665 ,23.9510591));
		list.add(new LatLng(54.9198548 ,23.951056 ));
		list.add(new LatLng(54.9198426 ,23.9510516));
		list.add(new LatLng(54.9198313 ,23.9510469));
		list.add(new LatLng(54.9198205 ,23.9510432));
		list.add(new LatLng(54.9198101 ,23.9510433));
		list.add(new LatLng(54.9197999 ,23.9510504));
		list.add(new LatLng(54.9197926 ,23.9510648));
		list.add(new LatLng(54.9197934 ,23.9510858));
		list.add(new LatLng(54.9198041 ,23.9511002));
		list.add(new LatLng(54.9198174 ,23.9511036));
		list.add(new LatLng(54.9198234 ,23.9510819));

		return list;
	}

}
