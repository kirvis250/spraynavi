package lt.minvib.spraynavig.spryfunction;

import java.util.List;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineSegment;

public class SprayingPolygonsFunc {



	public  final double multiplyerKoef = 1000.5; // tolerance koef of point displacement 
	public  final double bearingTolerance = 0.4; // tolerance of bearing
	private final int    maxPointsPerPolygon = 100; // maximum size of polygon

	private int      pointCounter =0;

	private LatLng[] lastLngMinusOne = null;
	private LatLng   beforeLastSprayed = null;
	private double   lastBearing  = 0.0;
	private LatLng[] newPair = null;
	private LatLng   myLocLatLng;
	private Location myloc;
	private boolean  isLastAreaTrengular = false;

	private int color = 0xB100FF00;
	private int color2= 0xB10FFF00;

	void debug(String a )
	{
		//Var.showDistance.setText(a);	
		//Var.showDistance.setVisibility(View.VISIBLE);
	}


	/* *************************************************************************
	 * Spraying logic 
	 * *************************************************************************/
	/*public void spray(Location myloc)
	{
		this.myloc = null;
		this.myloc  = myloc;

		myLocLatLng = new LatLng(myloc.getLatitude(), myloc.getLongitude());

		if(!myloc.hasBearing()) { myloc.setBearing((float)lastBearing); } // If lost bearing

		// Jeigu jau laikas fiksuot pur�kimo ta�kus
		if(SprVar.lastSprayedLoc == null ||   isItTimeToCalc(myLocLatLng ,SprVar.lastSprayedLoc, SprVar.sprayIntervalMeters))
		{
			newPair = null;
			newPair = new LatLng[] {SprVar.cCalc.linePointByBearingAndDistance(myloc, SprVar.atsumas/2, -90),
					SprVar.cCalc.linePointByBearingAndDistance(myloc, SprVar.atsumas/2, 90)};


			// naujas polygonas jei jis b�t� pirmas sara�e
			if(SprVar.lastSprayedLoc == null  || SprVar.sprayedPoly.size() <1 || SprVar.doAnewPolygon)
			{
				setSprayPolygonOptions( newPair[0],  newPair[1]);
				pointCounter = 0;
				lastLngMinusOne = null;
				updateVariables();
				SprVar.doAnewPolygon = false;
				debug("FIRST");

				// Naujas trikampis polygonas jei nauja linija kertasi su sena.
			} else if (SprVar.cCalc.isLinesIntersect(SprVar.lastPair, newPair )){

				setSprayPolygonOptions( SprVar.lastPair, newPair, this.lineIntersection(newPair, SprVar.lastPair));
				pointCounter = 0;
				lastLngMinusOne = null;
				updateVariables();
				debug( "if lines INTERSECT");

				// Naujas polygonas jei nu�oka nesveikai toli ta�kas   VALID
			} else if( !isPointsAtValidDistance(myLocLatLng ,SprVar.lastSprayedLoc, SprVar.sprayIntervalMeters)) {

				pointCounter = 0;
				setSprayPolygonOptions( newPair[0],   newPair[1]);
				lastLngMinusOne = null;
				debug( "if lines NOT VALID DISTANCE");
				updateVariables();

				//  naujas polygonas del to jei apsisuki ir pats per save persidengia arba turi perdaug ta�k�
			} else if(  pointCounter > maxPointsPerPolygon
					|| SprVar.cCalc.isPointInPolygon( SprVar.sprayedPoly.get(SprVar.sprayedPoly.size()-1).getPoints(), newPair[0]) 
					|| SprVar.cCalc.isPointInPolygon( SprVar.sprayedPoly.get(SprVar.sprayedPoly.size()-1).getPoints(), newPair[1]))
			{

				pointCounter = 0;
				setSprayPolygonOptions( SprVar.lastPair[0],   SprVar.lastPair[1]);
				lastLngMinusOne = null;
				debug( "IN POLYGON");
				updateVariables();

				// naujas polygonas jei magi�kai apsisukai vietoje
			} else if( beforeLastSprayed != null && BearingBetweenLines( new LatLng[] {this.beforeLastSprayed, SprVar.lastSprayedLoc },
					new LatLng[] {SprVar.lastSprayedLoc, myLocLatLng }) > 180
					&& BearingBetweenLines( new LatLng[] {this.beforeLastSprayed, SprVar.lastSprayedLoc },
							new LatLng[] {SprVar.lastSprayedLoc, myLocLatLng }) < 270){
				pointCounter = 0;
				setSprayPolygonOptions( SprVar.lastPair[0],   SprVar.lastPair[1]);
				lastLngMinusOne = null;
				debug( "if lines BEARING NOT VALID");
				updateVariables();

			} else if( diffrentkindOfIntersect(SprVar.lastPair , newPair ) != null ||
					( lastLngMinusOne != null &&  diffrentkindOfIntersect(lastLngMinusOne , newPair ) != null)) {

				pointCounter = 0;

				setSprayPolygonOptions(SprVar.lastPair ,newPair);

				lastLngMinusOne = null;
				debug( " DIFFERRENT KIND OF INTERSECTION");

				updateVariables();

			} else {
				if(isLastAreaTrengular)
				{						
					setSprayPolygonOptions( SprVar.lastPair, newPair); 

					updateVariables();

					pointCounter = 0;
					lastLngMinusOne = null;
					isLastAreaTrengular = false;

					debug( "AFTER TRIANGLE");

				} else {

					// Papildomas polygonas jei viskas gerai
					addPointsPairToPolygon( newPair[0],  newPair[1]);
					lastLngMinusOne = SprVar.lastPair;
					updateVariables();
					pointCounter++;	

					if( SprVar.lastPair != null 
							&& lastLngMinusOne != null
							&& SprVar.sprayedPoly.get(SprVar.sprayedPoly.size()-1).getPoints().size() >9  
							&& isLinesBearingEquals(SprVar.lastPair , newPair,  bearingTolerance)
							&& isLinesBearingEquals(newPair , lastLngMinusOne,  bearingTolerance))
					{
						this.deleteNotImportantPoints();
						pointCounter = pointCounter -2;
					}

					debug( "JUST UPDATE");
				}
			}
		}
	}

	// Update variables 
	public void updateVariables()
	{
		SprVar.lastPair = null;
		SprVar.lastPair = this.newPair;

		beforeLastSprayed  = null;
		beforeLastSprayed = SprVar.lastSprayedLoc;

		SprVar.lastSprayedLoc = null;
		SprVar.lastSprayedLoc = myLocLatLng;

		lastBearing = myloc.getBearing();
	}

	 *************************************************************************
	 * Set new polygon 
	 * ************************************************************************
	public void setSprayPolygonOptions(LatLng pt1, LatLng pt2)
	{
		SprVar.sprayedPolyOptions = new PolygonOptions()
		.fillColor(color)
		.strokeColor(color2)
		.strokeWidth(3)
		.geodesic(true)
		.add(pt1).add(pt2);
		SprVar.sprayedPoly.add( Var.mMap.addPolygon(SprVar.sprayedPolyOptions));


	}


	// set new polygon if intersecting
	public void setSprayPolygonOptions(LatLng [] line1 ,LatLng [] line2, LatLng intersect )
	{
		SprVar.sprayedPolyOptions = new PolygonOptions()
		.fillColor(color)
		.strokeColor(color2)
		.strokeWidth(3)
		.geodesic(true)
		.add(line1[0]).add(line2[0])
		.add(line1[1]);
		SprVar.sprayedPoly.add( Var.mMap.addPolygon(SprVar.sprayedPolyOptions));

		SprVar.sprayedPolyOptions = new PolygonOptions()
		.fillColor(color)
		.strokeColor(color2)
		.strokeWidth(3)
		.geodesic(true)
		.add(line1[0]).add(line2[1])
		.add(line1[1]);
		SprVar.sprayedPoly.add( Var.mMap.addPolygon(SprVar.sprayedPolyOptions));

		Var.sprayedPolyOptions = new PolygonOptions()
			.fillColor(color)
			.strokeColor(color2)
			.strokeWidth(3)
			.geodesic(true)
			.add(line1[0]).add(line1[1])
			.add(line2[1]);
			Var.sprayedPoly.add( Var.googleMap.addPolygon(Var.sprayedPolyOptions));


		//Var.sprayOptimizing.Optimyze(Var.sprayedPoly, Var.sprayedPoly.size()-3, Var.sprayedPoly.size()-2 );
		//Var.sprayOptimizing.Optimyze(Var.sprayedPoly, Var.sprayedPoly.size()-2, Var.sprayedPoly.size()-1 );  // TODO

		isLastAreaTrengular = true;
	}

	// set new polygon if intersecting in supper maner
	public void setSprayPolygonOptions(LatLng [] line1 ,LatLng [] line2 )
	{
		SprVar.sprayedPolyOptions = new PolygonOptions()
		.fillColor(color)
		.strokeColor(color2)
		.strokeWidth(3)
		.geodesic(true)
		.add(line1[0]).add(line2[1])
		.add(line2[0]);
		SprVar.sprayedPoly.add( Var.mMap.addPolygon(SprVar.sprayedPolyOptions));

		SprVar.sprayedPolyOptions = new PolygonOptions()
		.fillColor(color)
		.strokeColor(color2)
		.strokeWidth(3)
		.geodesic(true)
		.add(line1[0]).add(line1[1])
		.add(line2[1]);
		SprVar.sprayedPoly.add( Var.mMap.addPolygon(SprVar.sprayedPolyOptions));


		SprVar.sprayedPolyOptions = new PolygonOptions()
		.fillColor(color)
		.strokeColor(color2)
		.strokeWidth(3)
		.geodesic(true)
		.add(line1[1]).add(line2[0])
		.add(line2[1]);
		SprVar.sprayedPoly.add( Var.mMap.addPolygon(SprVar.sprayedPolyOptions));

		//Var.sprayOptimizing.Optimyze(Var.sprayedPoly, Var.sprayedPoly.size()-3, Var.sprayedPoly.size()-2 );
		//Var.sprayOptimizing.Optimyze(Var.sprayedPoly, Var.sprayedPoly.size()-2, Var.sprayedPoly.size()-1 );  // TODO

		isLastAreaTrengular = true;
	}


	 *************************************************************************
	 * After trengular polygon drawing
	 * ************************************************************************
	public void afterTrengular( LatLng [] line1 ,LatLng [] line2)
	{
		SprVar.sprayedPolyOptions = new PolygonOptions()
		.fillColor(color)
		.strokeColor(color2)
		.strokeWidth(3)
		.geodesic(true)
		.add(line1[0]).add(line1[1])
		.add(line2[0]).add(line2[1]);
		SprVar.sprayedPoly.add( Var.mMap.addPolygon(SprVar.sprayedPolyOptions));

	}

	 *************************************************************************
	 * Add points to existing polygon
	 * ************************************************************************
	public void addPointsPairToPolygon(LatLng pt1, LatLng pt2)
	{
		// get variables
		Polygon poly = SprVar.sprayedPoly.get(SprVar.sprayedPoly.size()-1);
		List<LatLng> coords = poly.getPoints();

		int possition = coords.size()/2;

		// adding
		coords.add(possition ,pt1);
		coords.add(possition+1,pt2);

		// returning values to list
		poly.setPoints(coords);
		SprVar.sprayedPoly.set(SprVar.sprayedPoly.size()-1, poly);	
	}

	 *************************************************************************
	 * Optimization of lines
	 * ************************************************************************
	public void deleteNotImportantPoints()
	{
		// get variables
		Polygon poly = SprVar.sprayedPoly.get(SprVar.sprayedPoly.size()-1);
		List<LatLng> coords = poly.getPoints();

		int possition = coords.size()/2;

		// actual removing
		coords.remove(possition-2);
		coords.remove(possition+1);

		// seting everything back
		poly.setPoints(coords);
		SprVar.sprayedPoly.set(SprVar.sprayedPoly.size()-1, poly);

		Log.i(" Points in polygon: ",   " count: " +String.valueOf(coords.size()));
	}

	 *************************************************************************
	 * Distance between point evaluation
	 * ************************************************************************
	public boolean isPointsAtValidDistance(LatLng pt1 , LatLng pt2, double distance)
	{ 
		double cmpDist = SprVar.cCalc.distanceBetweenPointsJTS(pt1, pt2); 
		return (cmpDist > distance &&  cmpDist  <  distance*multiplyerKoef);
	}

	// secondary Distance between point evaluation 
	public boolean isItTimeToCalc(LatLng pt1 , LatLng pt2, double distance)
	{ 
		double cmpDist = SprVar.cCalc.distanceBetweenPointsJTS(pt1, pt2); 
		return (cmpDist > distance);
	}

	 *************************************************************************
	 * Lines bearing evaluation
	 * ************************************************************************
	public boolean isLinesBearingEquals(LatLng[] p1 , LatLng[] p2, double tolerance)
	{
		double p1bearing =   SprVar.cCalc.lineSegmentBearing(p1[0], p1[1]);
		double p2bearing =   SprVar.cCalc.lineSegmentBearing(p2[0], p2[1]);
		return Math.abs(p1bearing - p2bearing) <= tolerance;
	}


	 *************************************************************************
	 * Lines bearing evaluation
	 * ************************************************************************
	public double BearingBetweenLines(LatLng[] p1 , LatLng[] p2)
	{
		double p1bearing =   SprVar.cCalc.lineSegmentBearing(p1[0], p1[1]);
		double p2bearing =   SprVar.cCalc.lineSegmentBearing(p2[0], p2[1]);
		return getDifference(p1bearing, p2bearing);
	}

	 *************************************************************************
	 * self intersection checking
	 * ************************************************************************
	public Coordinate diffrentkindOfIntersect(LatLng [] line1  , LatLng [] line2 )
	{
		Coordinate []Pt= {SprVar.cCalc.ToMercator(line1[0]),SprVar.cCalc.ToMercator(line1[1])};
		Coordinate []Nd= {SprVar.cCalc.ToMercator(line2[0]),SprVar.cCalc.ToMercator(line2[1])};

		LineSegment lineSeg1 = new LineSegment( Pt[1],  Nd[1]);
		LineSegment lineSeg2 = new LineSegment( Pt[0],  Nd[0]);

		return  lineSeg2.intersection(lineSeg1);
	}
	

	float getDifference(float a1, float a2) {
	    return Math.min((a1-a2)<0?a1-a2+360:a1-a2, (a2-a1)<0?a2-a1+360:a2-a1);
	}

	double getDifference(double a1, double a2) {
	    return Math.min((a1-a2)<0?a1-a2+360:a1-a2, (a2-a1)<0?a2-a1+360:a2-a1);
	}



	 *************************************************************************
	 * Line intersection checking
	 * ************************************************************************
	public LatLng lineIntersection(LatLng [] line1, LatLng[] line2 )
	{

		Coordinate [] coordinatep= {SprVar.cCalc.ToMercator(line1[0]),SprVar.cCalc.ToMercator(line1[1]),
				SprVar.cCalc.ToMercator(line2[0]),SprVar.cCalc.ToMercator(line2[1])};

		LineSegment lineSeg1 = new LineSegment(coordinatep[0], coordinatep[1]);
		LineSegment lineSeg2 = new LineSegment(coordinatep[2], coordinatep[3]);
		return SprVar.cCalc.ToLonLat(lineSeg1.intersection(lineSeg2));
	}


	 *************************************************************************
	 * Spray interval TODO
	 * ************************************************************************
	public void sprayInterval(Location loc)
	{
		if( loc.hasSpeed())
		{
			//SprVar.sprayIntervalMeters = loc.getSpeed() / SprVar.locationRenewRatio / 1000 ;
		}
	}
*/
}
