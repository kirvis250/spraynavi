package lt.minvib.spraynavig;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.Filter;
import android.widget.SimpleAdapter;

public class CustomAdapter extends SimpleAdapter {

    int resource;
    int[] to;

    List<Map<String, String>> data, mUnfilteredData;
    String[] from;

    Context context;
    Resources res;
    Filter filter;

    public CustomAdapter(Context context,
            List<Map<String, String>> data, int resource, String[] from, int[] to) {

        super(context, data, resource, from, to);
        this.context = context;
        this.resource = resource;
        this.data = data;
        this.from = from;
        this.to = to;
        this.res = context.getResources();
    }

    @Override
    public Filter getFilter() {
        if(filter != null) return filter;
        else return filter = new CustomFilter();
    }

    /**
     * CutomFiltras. Jei yra pasikartojančių žodžių list'e - nedubliuoja juo paie�?koje.
     */
    @SuppressLint("DefaultLocale")
    private class CustomFilter extends Filter {

        @SuppressLint("DefaultLocale")
        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            if (mUnfilteredData == null) {
                mUnfilteredData = new ArrayList<Map<String, String>>(data);
            }

            if (prefix == null || prefix.length() == 0) {
                List<Map<String, String>> list = mUnfilteredData;
                results.values = list;
                results.count = list.size();
            } else {
                String prefixString = prefix.toString().toLowerCase();
                List<Map<String, String>> unfilteredValues = mUnfilteredData;
                int count = unfilteredValues.size();
                ArrayList<Map<String, ?>> newValues = new ArrayList<Map<String, ?>>(count);

                for (int i = 0; i < count; i++) {
                    Map<String, ?> h = unfilteredValues.get(i);
                    if (h != null) {
                        int len = to.length;
                        for (int j=0; j<len; j++) {
                            String str =  (String)h.get(from[j]);
                          
                            String[] words = str.split(" ");
                            int wordCount = words.length;

                            for (int k = 0; k < wordCount; k++) {
                                String word = words[k];

                                if (word.toLowerCase().startsWith(prefixString)) { //žodis turi prasidėti nurodomais simboliais paie�?koje. Galima keistisi į .contains(prefix) - tada tikrins visą žodį
                                	if(!newValues.contains(h)) //!! Jei toks yra jau sąra�?e - jo neprideda
                                		newValues.add(h);
                                    
                                    break;
                                }
                            }
                        }
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
		@Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data.clear();
            data.addAll((List<Map<String, String>>) results.values);
            if (results.count > 0) {
                 notifyDataSetChanged();
            } else {
                 notifyDataSetInvalidated();
            }
        }
    }


}