package lt.minvib.spraynavig.navilines;

import com.google.android.gms.maps.model.LatLng;

public class ObjNavigationLines {
	
	private int id;
	private String title;
	private String fieldId;
	private String description;
	private long date;
	
	private LatLng latlng1;
	private LatLng latlng2;
	
	private String coordinates;
	
	
	public ObjNavigationLines(int id, String title, String fieldId, String description, long date,
			LatLng latlng1, LatLng latlng2, String coordinates) {
		
		this.id = id  ;
		this.title = title  ;
		this.fieldId =  fieldId ;
		this.description =  description ;
		this.date =  date ;
		
		this.latlng1 = latlng1  ;
		this.latlng2 =  latlng2 ;
		
		this.coordinates =  coordinates ;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public LatLng getLatlng1() {
		return latlng1;
	}

	public void setLatlng1(LatLng latlng1) {
		this.latlng1 = latlng1;
	}

	public LatLng getLatlng2() {
		return latlng2;
	}

	public void setLatlng2(LatLng latlng2) {
		this.latlng2 = latlng2;
	}

	public String getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

}
