package lt.minvib.spraynavig.navilines;

import com.google.android.gms.maps.model.LatLng;

import lt.minvib.spraynavig.DrawerActivity;
import lt.minvib.spraynavig.R;
import lt.minvib.spraynavig.fields.DatabaseFields;
import lt.minvib.spraynavig.fields.ObjField;
import lt.minvib.spraynavig.map.ActivityEditField;
import lt.minvib.spraynavig.map.Vr;
import lt.minvib.spraynavig.spryfunction.CorrdinateUtilities;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivitySaveNavigationLines extends DrawerActivity {
	
	
	EditText description_edit;
	EditText tittle_edit;
	Button save_button;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_add_navilines);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		description_edit = (EditText) findViewById(R.id.description_edit);
		tittle_edit = (EditText) findViewById(R.id.tittle_edit);
		save_button = (Button) findViewById(R.id.save_button);
		
		save_button.setOnClickListener(new  OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(tittle_edit.getText().toString().length() < 1)
				{
					Toast.makeText(ActivitySaveNavigationLines.this, "Title not entered", Toast.LENGTH_LONG).show();
					return;
				}
				
				String title = tittle_edit.getText().toString();
				String dssescription = description_edit.getEditableText().toString();
				
				
				int fieldid = Vr.selectedF.getId();

				CorrdinateUtilities cord = new   CorrdinateUtilities();

				LatLng a1 = Vr.p1;
				LatLng a2 = Vr.p2;

				Long date = System.currentTimeMillis();
			
				ObjNavigationLines navilines = new  ObjNavigationLines(0, title, 
						String.valueOf(fieldid), dssescription, date, a1, a2, "");
				
				DatabaseNavigationLines navi = new DatabaseNavigationLines(ActivitySaveNavigationLines.this);
				
				navi.addField(navilines);
				
				setResult(RESULT_OK);
				finish();
				
			}
		});
	}

}
