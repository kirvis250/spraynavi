package lt.minvib.spraynavig.navilines;

import java.util.ArrayList;
import java.util.List;

import lt.minvib.spraynavig.DB;
import lt.minvib.spraynavig.fields.ObjField;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.android.gms.maps.model.LatLng;

public class DatabaseNavigationLines {
	
	

	private Context ctx;


	public DatabaseNavigationLines( Context  ctx) {
		this.ctx = ctx;
	}
	
	
	
/*	 {
		 String CREATE_NAVI_TABLE = "CREATE TABLE " + TABLE_NABVIGATION_LINES + "(" 
				 + KEY_ID + " INTEGER PRIMARY KEY," 
				 + KEY_TITLE + " VARCHAR,"
				 + KEY_FIELD_ID + " VARCHAR,"
				 + KEY_DESCRIPTION + " VARCHAR,"
				 +  + " INTEGER,"
				 + KEY_COORDINATES + " VARCHAR)";
		 db.execSQL(CREATE_NAVI_TABLE); 
	 }
	*/
	
	public int addField(ObjNavigationLines obj)
	{
		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		ContentValues insertValues = new ContentValues();

		insertValues.put(DB.KEY_TITLE, obj.getTitle());
		insertValues.put(DB.KEY_FIELD_ID, obj.getFieldId());
		insertValues.put(DB.KEY_DESCRIPTION , obj.getDescription());
		insertValues.put(DB.KEY_DATE, obj.getDate());
		insertValues.put(DB.KEY_COORDINATES,  LatLngToString(obj.getLatlng1(),obj.getLatlng2()));

		long newId = db.insert(DB.TABLE_NABVIGATION_LINES, null, insertValues);
 
		return (int)newId;	
	}
	

	
	
	
	
	public List<ObjNavigationLines> getAllNavigation( int idd)
	{
		
		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		Cursor cursor = null;

		List<ObjNavigationLines> fields = new ArrayList<ObjNavigationLines>();

		 cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_NABVIGATION_LINES + " WHERE "+ DB.KEY_FIELD_ID +"="+idd+" ;", null);


		cursor.moveToFirst();
		while (cursor.isAfterLast()== false) {

			
/*			private String fieldId;
			private String description;
			private long date;
			
			private LatLng latlng1;
			private LatLng latlng2;
			
			private String coordinates;*/
			
			
			int id = cursor.getInt(0);
			String title = cursor.getString(1);
			String fieldId  = cursor.getString(2);
			String description  = cursor.getString(3);
			long date = cursor.getLong(4);
			
			String coordinates = cursor.getString(5);
			LatLng[] aa = StrToLatLng(coordinates);
			
			LatLng latlng1 = aa[0];
			LatLng latlng2 = aa[1];
			
		ObjNavigationLines navi = new ObjNavigationLines(id, title, fieldId, description,
				date, latlng1, latlng2, coordinates);
		
			fields.add(navi);	

			cursor.moveToNext();
		}
		cursor.close();
		return fields;
	
	}
	
	
	
	public ObjNavigationLines getfield(int id)
	{
		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		Cursor cursor = null;

		ObjNavigationLines navi = null;

		 cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_NABVIGATION_LINES + " WHERE "+ DB.KEY_ID +"=" + id + " ;", null);

		cursor.moveToFirst();
		if(cursor.isAfterLast()== false) {
			
			int iad = cursor.getInt(0);
			String title = cursor.getString(1);
			String fieldId  = cursor.getString(2);
			String description  = cursor.getString(3);
			long date = cursor.getLong(4);
			
			String coordinates = cursor.getString(5);
			LatLng[] aa = StrToLatLng(coordinates);
			
			LatLng latlng1 = aa[0];
			LatLng latlng2 = aa[1];
			
		 navi = new ObjNavigationLines(id, title, fieldId, description,
				date, latlng1, latlng2, coordinates);
		}
		cursor.close();
		return navi;
		
		
		
		
	}
	
	
	public static String LatLngToString( LatLng l1, LatLng l2)
	{
		String returnable = "";
	
		returnable  = returnable + l1.latitude + "&"+ l1.longitude + "&";
		returnable  = returnable + l2.latitude + "&"+ l2.longitude + "&";
		
		returnable = returnable.substring(0, returnable.length()-1);
		return returnable;
	}
	
	public static  LatLng[] StrToLatLng(String parsable)
	{
		String [] arr = parsable.split("&");
		
		List<LatLng> list = new ArrayList<LatLng>();
		
		for(int i = 0; i < arr.length -1 ; i=i+2)
		{
			LatLng lat = new LatLng( Double.valueOf( arr[i]),  Double.valueOf( arr[i+1]));
			list.add(lat);
		}
		
		LatLng [] a = {list.get(0), list.get(1)};
		return a;
	}
	
	
	
	
	
	
/*	 				 + KEY_ID + " INTEGER PRIMARY KEY," 
				 + KEY_NAME + " VARCHAR,"
				 + KEY_AREA_1 + " REAL," 
				 + KEY_AREA_FROM_MAP + " REAL," 
				 + KEY_GROUP + " INTEGER," 
				 + KEY_FIELD_NUMBER + " VARCHAR," 
				 + KEY_COORDINATES + " VARCHAR, " 
				 + KEY_UNIQUE_NUMBER +" INTEGER, "
				 + KEY_DELETED + " INTEGER DEFAULT 0)";*/

	 
	/** paima klausimą pagal jo telefono pusės id 
	 * 
	 * @param id - telefono pusės id
	 * @return - obiektas
	 *//*
	public ObjSos getSosById(int id)
	{
		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		Cursor cursor = null;
		ObjSos problem = null;


		cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_SOS + " WHERE "
				+ DB.SOS_ID + " = " + id + " ;", null);


		if(cursor.moveToFirst()) {

			int iid  = cursor.getInt(0);
			int farmeId = cursor.getInt(1);
			String str = cursor.getString(2);
			int cate = cursor.getInt(3);
			int diag = cursor.getInt(4);
			int prid = cursor.getInt(5);
			String comment =  cursor.getString(6);

			int fieldId = cursor.getInt(7);
			int ans = cursor.getInt(8);
			int rew = cursor.getInt(9);
			String urls = cursor.getString(10);
			String IntUrls = cursor.getString(11);
			String names  = cursor.getString(12);
			Long date = cursor.getLong(13);

			int objId = cursor.getInt(14);
			
			int isProblems = cursor.getInt(15);
			boolean deleted  = cursor.getInt(16) == 1;

			problem  = new ObjSos( iid, farmeId, str, cate, diag, prid, comment,
					ans > 0, rew > 0, fieldId, resolveUrls(urls), resolveUrls(IntUrls), resolveUrls(names), date, objId,isProblems , deleted);

		}
		cursor.close();
		return problem;
	}



	*//** paima klausimą pagal jo serverio pusės id
	 * 
	 * @param id - serverio pusės id
	 * @return - obiektas
	 *//*
	public ObjSos getSosByObjId( int id)
	{
		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		Cursor cursor = null;
		ObjSos problem = null;


		cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_SOS + " WHERE "
				+ DB.SOS_OBJ_ID + " = " + id + " ;", null);


		if(cursor.moveToFirst()) {

			int iid  = cursor.getInt(0);
			int farmeId = cursor.getInt(1);
			String str = cursor.getString(2);
			int cate = cursor.getInt(3);
			int diag = cursor.getInt(4);
			int prid = cursor.getInt(5);
			String comment =  cursor.getString(6);

			int fieldId = cursor.getInt(7);
			int ans = cursor.getInt(8);
			int rew = cursor.getInt(9);
			String urls = cursor.getString(10);
			String IntUrls = cursor.getString(11);
			String names  = cursor.getString(12);
			Long date = cursor.getLong(13);

			int objId = cursor.getInt(14);
			
			
			int isProblems = cursor.getInt(15);
			boolean deleted  = cursor.getInt(16) == 1;

			problem  = new ObjSos( iid, farmeId, str, cate, diag, prid, comment,
					ans > 0, rew > 0, fieldId, resolveUrls(urls), resolveUrls(IntUrls), resolveUrls(names), date, objId,isProblems , deleted);
		}
		cursor.close();
		return problem;
	}


	*//** gauna visas problemas
	 * 
	 * @param farmerId
	 * @return
	 *//*
	public List<ObjSos> getSOSAll( )
	{

		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		Cursor cursor = null;
		ObjSos problem;

		List<ObjSos> problems = new ArrayList<ObjSos>();

		            cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_SOS + " WHERE "
                    + DB.SOS_FARMER_ID + " = " + farmerId + " ORDER BY "+ DB.SOS_DATE +" DESC ;", null);

		cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_SOS + " ORDER BY "+ DB.SOS_DATE +" DESC ;", null);

		cursor.moveToFirst();
		while (cursor.isAfterLast()== false) {

			int iid  = cursor.getInt(0);
			int farmeId = cursor.getInt(1);
			String str = cursor.getString(2);
			int cate = cursor.getInt(3);
			int diag = cursor.getInt(4);
			int prid = cursor.getInt(5);
			String comment =  cursor.getString(6);

			int fieldId = cursor.getInt(7);
			int ans = cursor.getInt(8);
			int rew = cursor.getInt(9);
			String urls = cursor.getString(10);
			String IntUrls = cursor.getString(11); 
			String names = cursor.getString(12);  
			Long date = cursor.getLong(13);

			int objId = cursor.getInt(14);


			int isProblems = cursor.getInt(15);
			boolean deleted  = cursor.getInt(16) == 1;

			problem  = new ObjSos( iid, farmeId, str, cate, diag, prid, comment,
					ans > 0, rew > 0, fieldId, resolveUrls(urls), resolveUrls(IntUrls), resolveUrls(names), date, objId,isProblems , deleted);

			problems.add(problem);	

			cursor.moveToNext();
		}
		cursor.close();
		return problems;
	}

	
	*//** graÅ¾ina true, jei yra neatsakytÅ³ problemÅ³
	 *  
	 * @return - true arba false
	 *//*
	public boolean isUnansweredProblemsExist()
	{

		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		Cursor cursor = null;

		cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_SOS + " WHERE "
				+ DB.SOS_ANSWERED + " = -1;", null);

		if(cursor.moveToFirst()) {
				cursor.close();
				return true;
		} else {
			cursor.close();
			return false;
		}
	}




	*//*
	private String urlListToString(List<String> urls)
	{
		String returnable = "";
		if(urls != null && urls.size() > 0)
			for(String a : urls)
			{
				returnable = returnable + a +" @ ";
			}

		return returnable;
	}

	
	*//** iƫrapƴo lauko vardą 
	 * 
	 * @param fieldid - lauko id
	 * @param ct - kontekstas
	 * @return - lauko vardas arba R.string.field_is_unknown, jeigu laukas nerastas
	 *//*
	public static String resolveFieldName( int fieldid, Context ct)
	{
		if(fieldid > -1){
			DatabaseFields fields = new DatabaseFields(ct);

			ObjField field = fields.getFieldById(fieldid);

			if(field.getName()== null)
			{
				return ct.getString(R.string.field_is_unknown);
			}
			return field.getName() +", "+ field.getSize() + " ha";
		}
		return "";
	}
	
	
	*//** iƫrapƴo lauką pagal jo id - reikėtų kažkur Ʃtą perkelt
	 * 
	 * @param fieldid - lauko id
	 * @param ct	- kontekstas
	 * @return	- Lauko objektas
	 *//* 
	public static ObjField resolveField( int fieldid, Context ct)
	{
		if(fieldid > -1){
			DatabaseFields fields = new DatabaseFields(ct);

			ObjField field = fields.getFieldById(fieldid);

			if(field.getName()== null)
			{
				return null;  
			}
			return field;
		}
		return null;
	}

	*/

}
