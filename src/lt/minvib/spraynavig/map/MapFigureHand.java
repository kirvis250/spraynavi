package lt.minvib.spraynavig.map;

import java.util.ArrayList;
import java.util.List;

import lt.minvib.spraynavig.R;
import lt.minvib.spraynavig.fields.DatabaseFields;
import lt.minvib.spraynavig.fields.ObjField;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

public class MapFigureHand {
	
	
	
	
	/** removes given markers form map
	 * 
	 * @param list
	 */
	public static void deleteMarkers (List<Marker> list)
	{
		if(list != null && list.size() >0)
			for(int i = 0; i < list.size(); i++)
			{
				list.get(i).remove();
			}
	}
	
	

	public static void removeLastAreaMarker(Context ctx, PolygonOptions opts){
		
		if(ActivityMap.marks.size() > 0)
		{
			Marker a = ActivityMap.marks.get(ActivityMap.marks.size()-1);
			
			if(ActivityMap.area != null && ActivityMap.area.getPoints().size() > 0)
			{

				List<LatLng> lats = ActivityMap.area.getPoints();
				
				if(lats.size() > 3){
			  	   ActivityMap.marks.remove(ActivityMap.marks.size()-1);
				   a.remove();
				   lats.remove( lats.size()-3);
				   ActivityMap.area.setPoints(lats);
				}
				else if( lats.size() == 3){
					ActivityMap.area.remove();
					deleteMarkers(ActivityMap.marks);
					ActivityMap.area = null;
					ActivityMap.marks = new ArrayList<Marker>();
					mapClickCreatingAmap(lats.get(lats.size()-2),  ctx,  opts);
					}
				else if(lats.size() <= 2){
					ActivityMap.area.remove();
					deleteMarkers(ActivityMap.marks);
					ActivityMap.area = null;
					ActivityMap.marks = new ArrayList<Marker>();
				}
			} else {
				if(ActivityMap.area!= null)
					ActivityMap.area.remove();
				deleteMarkers(ActivityMap.marks);
				ActivityMap.area = null;
				ActivityMap.marks = new ArrayList<Marker>();
			}
		} else {
			if(ActivityMap.area!= null)
				ActivityMap.area.remove();
			deleteMarkers(ActivityMap.marks);
			ActivityMap.area = null;
			ActivityMap.marks = new ArrayList<Marker>();
		}
	}
	
	
	
	/** removes given polygon form map
	 * 
	 * @param list
	 */
	public static void deletePoly (Polygon poly)
	{
		if(poly != null)
			poly.remove();
	}
	
	
	public static PolygonOptions polygonOps(Context ctx, boolean isSelected)
	{
		PolygonOptions opts = new PolygonOptions();

						opts.strokeWidth(4);
						
						if(isSelected)
							opts.strokeColor(ctx.getResources().getColor(R.color.MapPolygonStrokeSelected));
						else
							opts.strokeColor(ctx.getResources().getColor(R.color.MapPolygonStroke));
							
						opts.fillColor(ctx.getResources().getColor(R.color.MapPolygonFill));
		
		return opts;
	}
	
	
	
	public static void MapClickOriginal(LatLng point ,Functions func, Context ctx )
	{
		boolean none = true;
		if(Vr.polys != null){
			for(int i = 0; i< Vr.polys.size(); i++)
				if(func.arDaugiakampy(point,  Vr.polys.get(i).getPoints()))
				{
					MapFigureHand.selectField(i, ctx);
					none = false;
				}
			if(none)
			{
				MapFigureHand.selectField(ctx);
			}
		}
	}
	
	public static int getFieldposition(String id)
	{
		if(Vr.polys != null){
			for(int i = 0; i< Vr.polys.size(); i++)
				if( Vr.polys.get(i).getId().equalsIgnoreCase(id) )
				{
					return i;
				
				}
		}
		return -1;
		
	}
	
	
	
	public static void selectField(int field_id, Context ctx)
	{
		if(Vr.selected != -1)
			Vr.polys.get(Vr.selected).setStrokeColor(ctx.getResources().getColor(R.color.MapPolygonStroke));
		
		 Vr.poly =  Vr.polys.get(field_id);
		 Vr.poly.setStrokeColor(ctx.getResources().getColor(R.color.MapPolygonStrokeSelected));
		 Vr.selected = field_id;
		 Vr.selectedF = Vr.polysF.get(field_id);
	}
	
	
	public static void selectField( Context ctx)
	{
		if(Vr.selected != -1)
			Vr.polys.get(Vr.selected).setStrokeColor(ctx.getResources().getColor(R.color.MapPolygonStroke));
		Vr.selected  = -1;
	}
	
	
	public static void mapClickCreatingAmap(LatLng point, Context ctx, PolygonOptions opts)
	{

		MarkerOptions mark1 = new MarkerOptions().visible(true);
		mark1.position(point);

		ActivityMap.marks.add(Vr.map.addMarker(mark1));

		if(ActivityMap.marks.size() > 1)
		{
			if(ActivityMap.area == null)
			{
				opts = new PolygonOptions();
				opts.add(point);
				opts.add(ActivityMap.marks.get(0).getPosition());
				opts.strokeWidth(4);
				opts.strokeColor(ctx.getResources().getColor(R.color.MapPolygonStroke));
				opts.fillColor(ctx.getResources().getColor(R.color.MapPolygonFill));
				ActivityMap.area = Vr.map.addPolygon(opts);
			} else {

				List<LatLng> lats = ActivityMap.area.getPoints();
				lats.add( lats.size()-2,point );
				ActivityMap.area.setPoints(lats);
			}
		}
	}
	
	
	
	
	
	
	public static void addAllPolygonsToMap(Context ctx)
	{
		
		if(Vr.polys != null)
		{
			for(int i = 0; i < Vr.polys.size(); i++)
			{
				Vr.polys.get(i).remove();
			}
			Vr.polys = null;
		}
		
		DatabaseFields dbc = new DatabaseFields(ctx);
		
		Vr.polysF = dbc.getAllFields();
		Vr.polys = new ArrayList<Polygon>();
		
		for(int i = 0; i < Vr.polysF.size(); i++)
		{
			ObjField field = Vr.polysF.get(i);
			
			PolygonOptions opts = polygonOps(ctx, false);
			opts.addAll(field.getCoordinates());
			
			Vr.polys.add( Vr.map.addPolygon(opts));
			
		}
		
		
		
	}
	
	
	
	

}
