package lt.minvib.spraynavig.map;

public class ChooseModeItem{
    public String text;
    public int icon;
    
    public ChooseModeItem(String text, Integer icon) {
        this.text = text;
        this.icon = icon;
    }
    
    @Override
    public String toString() {
        return text;
    }
}