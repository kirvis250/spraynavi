package lt.minvib.spraynavig.map;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import lt.minvib.spraynavig.R;
import lt.minvib.spraynavig.fields.ObjField;
import android.content.Context;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;

public class Functions {

    private static final float AREA_ACRA = 0.0002471054F;
    private static final float DIS_FT = 3.28084F;
    private static final float AREA_FT2 = 10.76391F;
    private static final float AREA_HA = 1.0E-004F;
    private static final float DIS_KM = 0.001F;
    private static final float AREA_KM2 = 1.0E-006F;
    private static final float DIS_YD = 1.093613F;
    private static final float AREA_YD2 = 1.19599F;

    public void copyPointsToUndo(List<Marker> taskai, GoogleMap mMap, LinkedList<List<Marker>> undo) {
        // daro tasku kopijas undo funcijai
        List<Marker> taskai2 = new ArrayList<Marker>();
        Marker taskas;
        for (Marker a : taskai) {
            MarkerOptions opt = new MarkerOptions();
            opt.draggable(false);
            opt.snippet(a.getSnippet());
            opt.position(a.getPosition());
            opt.visible(false);
            opt.anchor(0.5f, 0.5f);
            taskas = mMap.addMarker(opt);
            taskai2.add(taskas);
        }
        undo.add(new ArrayList<Marker>(taskai2));
    }

/*    public void pridetiTarpinius(List<Marker> taskai, GoogleMap mMap) {
        // prideda tarpinius taskus tarp 2 tikru tasku
        MarkerOptions opt = new MarkerOptions();
        opt.position(vidurioTaskas(taskai.get(0).getPosition(), taskai.get(1).getPosition()));
        opt.snippet("Tarpinis");
        opt.icon(BitmapDescriptorFactory.fromResource(R.drawable.tarpinis));
        opt.anchor(0.5f, 0.5f);
        taskai.add(1, mMap.addMarker(opt));

        opt.position(vidurioTaskas(taskai.get(2).getPosition(), taskai.get(3).getPosition()));
        opt.snippet("Tarpinis");
        opt.icon(BitmapDescriptorFactory.fromResource(R.drawable.tarpinis));
        taskai.add(3, mMap.addMarker(opt));

        opt.position(vidurioTaskas(taskai.get(4).getPosition(), taskai.get(0).getPosition()));
        opt.snippet("Tarpinis");
        opt.icon(BitmapDescriptorFactory.fromResource(R.drawable.tarpinis));
        opt.anchor(0.5f, 0.5f);
        taskai.add(5, mMap.addMarker(opt));
    }*/

    public LatLng vidurioTaskas(LatLng pirmas, LatLng antras) { // suranda
                                                                // vidurio taska
                                                                // tarp dvieju
                                                                // koordinaciu
        // suskaiciuoja viduri tarp 2 tikru tasku
        double X1, Y1, Z1, X2, Y2, Z2, x, y, z, lon, hyp, lat;

        X1 = Math.cos(Math.toRadians(pirmas.latitude)) * Math.cos(Math.toRadians(pirmas.longitude));
        Y1 = Math.cos(Math.toRadians(pirmas.latitude)) * Math.sin(Math.toRadians(pirmas.longitude));
        Z1 = Math.sin(Math.toRadians(pirmas.latitude));

        X2 = Math.cos(Math.toRadians(antras.latitude)) * Math.cos(Math.toRadians(antras.longitude));
        Y2 = Math.cos(Math.toRadians(antras.latitude)) * Math.sin(Math.toRadians(antras.longitude));
        Z2 = Math.sin(Math.toRadians(antras.latitude));

        x = (X1 + X2) / 2;
        y = (Y1 + Y2) / 2;
        z = (Z1 + Z2) / 2;

        lon = Math.atan2(y, x);
        hyp = Math.sqrt(x * x + y * y);
        lat = Math.atan2(z, hyp);

        LatLng taskas = new LatLng(Math.toDegrees(lat), Math.toDegrees(lon));
        return taskas;
    }

    public boolean isMarkerTouched(Point taskas, Point taskas2, int xdpi, int ydpi, int ydpi2) {
        if ((taskas.x - taskas2.x < xdpi / 1.8) && (taskas.x - taskas2.x > (-xdpi / 1.8))
                && (taskas.y - taskas2.y < ydpi) && (taskas.y - taskas2.y > ydpi2))
            return true;
        return false;
    }

    // iterpia tarpinius markerius redaguojant ar spaudziant +
  /*  public void iterpti(Marker mark, List<Marker> taskai, GoogleMap mMap) {
        int kur = taskai.indexOf(mark);
        int kur2 = kur;
        MarkerOptions opt = new MarkerOptions();
        if (kur == 0)
            opt.position(vidurioTaskas(taskai.get(taskai.size() - 1).getPosition(), mark.getPosition()));
        else
            opt.position(vidurioTaskas(taskai.get(kur - 1).getPosition(), mark.getPosition()));
        opt.snippet("Tarpinis");
        opt.icon(BitmapDescriptorFactory.fromResource(R.drawable.tarpinis));
        opt.anchor(0.5f, 0.5f);
        if (kur == 0)
            taskai.add(taskai.size(), mMap.addMarker(opt));
        else
            taskai.add(kur, mMap.addMarker(opt));

        if (kur == taskai.size() - 2)
            kur2 = -2;
        if (kur == 0)
            kur2 = -1;
        opt.position(vidurioTaskas(mark.getPosition(), taskai.get(kur2 + 2).getPosition()));
        opt.snippet("Tarpinis");
        opt.icon(BitmapDescriptorFactory.fromResource(R.drawable.tarpinis));
        opt.anchor(0.5f, 0.5f);
        if (kur == 0)
            taskai.add(kur + 1, mMap.addMarker(opt));
        else
            taskai.add(kur + 2, mMap.addMarker(opt));
        mark.setSnippet("Tikras");
    }

    // iterpia markerius deliojant taskus
    public void iterpti2(Marker mark, List<Marker> taskai, GoogleMap mMap) {
        int kur = taskai.indexOf(mark);
        int kur2 = kur;
        MarkerOptions opt = new MarkerOptions();
        opt.position(vidurioTaskas(taskai.get(kur - 2).getPosition(), mark.getPosition()));
        opt.snippet("Tarpinis");
        opt.icon(BitmapDescriptorFactory.fromResource(R.drawable.tarpinis));
        taskai.get(kur - 1).remove();
        opt.anchor(0.5f, 0.5f);
        taskai.set(kur - 1, mMap.addMarker(opt));
        if (kur == taskai.size() - 1)
            kur2 = -1;
        opt.position(vidurioTaskas(mark.getPosition(), taskai.get(kur2 + 1).getPosition()));
        opt.snippet("Tarpinis");
        opt.icon(BitmapDescriptorFactory.fromResource(R.drawable.tarpinis));
        opt.anchor(0.5f, 0.5f);
        taskai.add(kur + 1, mMap.addMarker(opt));
        mark.setSnippet("Tikras");
    }*/

    // ===========================================================================================
    // GPS matavimo
    public List<LatLng> kampuApvalinimas(List<LatLng> taskai) {
        double t, ax, ay, bx, by, cx, cy, dx, dy, lat, lon;
        int i;
        List<LatLng> points = new ArrayList<LatLng>();
        sutvarkytiTaskus(taskai);
        for (i = 2; i < taskai.size() - 2; i++) {
            for (t = 0; t < 1; t += 0.2) {
                ax = (-(taskai.get(i - 2).latitude) + 3 * taskai.get(i - 1).latitude - 3 * taskai.get(i).latitude + taskai
                        .get(i + 1).latitude) / 6;
                ay = (-(taskai.get(i - 2).longitude) + 3 * taskai.get(i - 1).longitude - 3 * taskai.get(i).longitude + taskai
                        .get(i + 1).longitude) / 6;
                bx = (taskai.get(i - 2).latitude - 2 * taskai.get(i - 1).latitude + taskai.get(i).latitude) / 2;
                by = (taskai.get(i - 2).longitude - 2 * taskai.get(i - 1).longitude + taskai.get(i).longitude) / 2;
                cx = (-(taskai.get(i - 2).latitude) + taskai.get(i).latitude) / 2;
                cy = (-(taskai.get(i - 2).longitude) + taskai.get(i).longitude) / 2;
                dx = (taskai.get(i - 2).latitude + 4 * taskai.get(i - 1).latitude + taskai.get(i).latitude) / 6;
                dy = (taskai.get(i - 2).longitude + 4 * taskai.get(i - 1).longitude + taskai.get(i).longitude) / 6;
                lat = ax * Math.pow(t + 0.1, 3) + bx * Math.pow(t + 0.1, 2) + cx * (t + 0.1) + dx;
                lon = ay * Math.pow(t + 0.1, 3) + by * Math.pow(t + 0.1, 2) + cy * (t + 0.1) + dy;
                points.add(new LatLng(lat, lon));
            }
        }
        return points;
    }

    // sutvarko taskus kad butu galima tiksliau suapvalinti linija
    private void sutvarkytiTaskus(List<LatLng> taskai) {
        taskai.add(0, taskai.get(taskai.size() - 2));
        taskai.add(0, taskai.get(taskai.size() - 1));
        taskai.add(taskai.get(0));
        taskai.add(taskai.get(1));
    }

    // Gauna LatLng is Location kintamojo
    public LatLng locationToLatLng(Location loc) {
        if (loc != null)
            return new LatLng(loc.getLatitude(), loc.getLongitude());
        return null;
    }

    // ======================================================================================
    // perimetras //linija uzdara arba ne
    public double perimetras(List<LatLng> taskai, boolean sujungti) {
        // visu krastiniu perimetras
        double distance = 0;
        for (int i = 0; i < taskai.size() - 1; i++) {
            distance += atstumas(taskai.get(i), taskai.get(i + 1));
        }
        if (taskai.size() > 2 && sujungti) {
            distance += atstumas(taskai.get(taskai.size() - 1), taskai.get(0));
        }
        return distance;
    }

    public double atstumas(LatLng taskas, LatLng taskas2) {
        // atstumas nuo vieno tasko iki kito
        final int R = 6371; // Zemes spindulys
        double distance = 0;
        Double latDistance = deg2rad(taskas2.latitude - taskas.latitude);
        Double lonDistance = deg2rad(taskas2.longitude - taskas.longitude);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(deg2rad(taskas.latitude))
                * Math.cos(deg2rad(taskas2.latitude)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        distance += R * c;
        // / double height = el1 - el2; //altitude
        // distance = Math.pow(distance, 2) + Math.pow(height, 2);
        // return Math.sqrt(distance);
        // Log.d("dd","distancija"+distance);
        return distance * 1000;
    }

    // ================================================================================
    // laipsniai i radianus
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    // radianai i lapsinius
    /*
     * private double rad2deg(double rad) { return (rad * 180.0 / Math.PI); }
     */
    // ================================================================================
    // vercia ilguma/platuma i Dekarto koord. sistema
    public double havX(double lat, double lon) {
        final int R = 6371;
        double x = R * Math.cos(lat) * Math.cos(lon);
        return x;
    }

    public double havY(double lat, double lon) {
        final int R = 6371;
        double y = R * Math.cos(lat) * Math.sin(lon);
        return y;
    }

    // ==================================================================================
    // tikrina ar daugiakampyje yra susikertanciu krastiniu
    public boolean susikirtimas(List<Marker> taskai) {
        int j = taskai.size() - 1;
        double X1 = 0;
        double Y1 = 0;
        double X2 = 0;
        double Y2 = 0;
        double X3 = 0;
        double Y3 = 0;
        double X4 = 0;
        double Y4 = 0;
        for (int i = 1; i < taskai.size() - 3; i++) {
            X1 = havX(taskai.get(j).getPosition().latitude, taskai.get(j).getPosition().longitude);
            X2 = havX(taskai.get(i).getPosition().latitude, taskai.get(i).getPosition().longitude);

            Y1 = havY(taskai.get(j).getPosition().latitude, taskai.get(j).getPosition().longitude);
            Y2 = havY(taskai.get(i).getPosition().latitude, taskai.get(i).getPosition().longitude);

            for (int k = i + 1; k < taskai.size() - 1; k++) {
                X3 = havX(taskai.get(k).getPosition().latitude, taskai.get(k).getPosition().longitude);
                Y3 = havY(taskai.get(k).getPosition().latitude, taskai.get(k).getPosition().longitude);
                X4 = havX(taskai.get(k + 1).getPosition().latitude, taskai.get(k + 1).getPosition().longitude);
                Y4 = havY(taskai.get(k + 1).getPosition().latitude, taskai.get(k + 1).getPosition().longitude);
                if (linesIntersect(X1, Y1, X2, Y2, X3, Y3, X4, Y4))
                    return true;
                if (j == taskai.size() - 1 && k == taskai.size() - 3)
                    k = taskai.size();
            }
            j = i;
        }
        return false;
    }

    // tikrina ar 2 linijos kertasi
    public static boolean linesIntersect(double x1, double y1, double x2, double y2, double x3, double y3, double x4,
            double y4) {
        if (x1 == x2 && y1 == y2 || x3 == x4 && y3 == y4)
            return false;

        double ax = x2 - x1;
        double ay = y2 - y1;
        double bx = x3 - x4;
        double by = y3 - y4;
        double cx = x1 - x3;
        double cy = y1 - y3;

        double alphaNumerator = by * cx - bx * cy;
        double commonDenominator = ay * bx - ax * by;
        if (commonDenominator > 0) {
            if (alphaNumerator < 0 || alphaNumerator > commonDenominator) {
                return false;
            }
        } else if (commonDenominator < 0) {
            if (alphaNumerator > 0 || alphaNumerator < commonDenominator) {
                return false;
            }
        }
        double betaNumerator = ax * cy - ay * cx;
        if (commonDenominator > 0) {
            if (betaNumerator < 0 || betaNumerator > commonDenominator) {
                return false;
            }
        } else if (commonDenominator < 0) {
            if (betaNumerator > 0 || betaNumerator < commonDenominator) {
                return false;
            }
        }
        if (commonDenominator == 0) {
            double y3LessY1 = y3 - y1;
            double collinearityTestForP3 = x1 * (y2 - y3) + x2 * (y3LessY1) + x3 * (y1 - y2);
            if (collinearityTestForP3 == 0) {
                if (x1 >= x3 && x1 <= x4 || x1 <= x3 && x1 >= x4 || x2 >= x3 && x2 <= x4 || x2 <= x3 && x2 >= x4
                        || x3 >= x1 && x3 <= x2 || x3 <= x1 && x3 >= x2) {
                    if (y1 >= y3 && y1 <= y4 || y1 <= y3 && y1 >= y4 || y2 >= y3 && y2 <= y4 || y2 <= y3 && y2 >= y4
                            || y3 >= y1 && y3 <= y2 || y3 <= y1 && y3 >= y2) {
                        return true;
                    }
                }
            }
            return false;
        }
        return true;
    }

    // ==================================================================================
    public List<LatLng> ListMarkerToListLatLng(List<Marker> taskai) {
        List<LatLng> koord = new ArrayList<LatLng>();
        for (Marker a : taskai)
            koord.add(a.getPosition());
        return koord;
    }

/*    public void kmlExportavimas(List<LatLng> taskai, String name, String perimeter, String area, boolean poly,
            Context ctx) throws FileNotFoundException, IOException {
        KMLexporter kmlEx = new KMLexporter();
        // File localFile1 = new File(Environment.getExternalStorageDirectory(),
        // "/KML.kml");
        File localFile1 = new File(ctx.getFilesDir(), "/KML.kml");
        kmlEx.export(name, perimeter, area, taskai, new FileOutputStream(localFile1), poly);

    }*/

    public boolean arDaugiakampy(LatLng tap, List<LatLng> list) {
        int intersectCount = 0;
        for (int j = 0; j < list.size() - 1; j++) {
            if (rayCastIntersect(tap, list.get(j), list.get(j + 1))) {
                intersectCount++;
            }
        }

        return ((intersectCount % 2) == 1); // nelyginis = viduj, lyginis = iÅ�orÄ—j
                                           
    }

    private boolean rayCastIntersect(LatLng tap, LatLng vertA, LatLng vertB) {

        double aY = vertA.latitude;
        double bY = vertB.latitude;
        double aX = vertA.longitude;
        double bX = vertB.longitude;
        double pY = tap.latitude;
        double pX = tap.longitude;

        if ((aY > pY && bY > pY) || (aY < pY && bY < pY) || (aX < pX && bX < pX)) {
            return false;
        }

        double m = (aY - bY) / (aX - bX);
        double bee = (-aX) * m + aY; // y = mx + b
        double x = (pY - bee) / m;

        return x > pX;
    }

    public double areaCalc(List<LatLng> taskai) {
        Log.d("fff", "taskai " + taskai.size());
        if (taskai.size() < 3)
            return 0;
        double d1 = 0.0D;
        LatLng taskas = taskai.get(0);
        Coordinate[] koordinates = new Coordinate[1 + taskai.size()];
        for (int i = 0; i < taskai.size(); i++)
            koordinates[i] = new Coordinate(taskai.get(i).longitude, taskai.get(i).latitude);
        koordinates[taskai.size()] = koordinates[0];
        GeometryFactory localGeometryFactory = new GeometryFactory();
        Polygon localPolygon = localGeometryFactory.createPolygon(localGeometryFactory.createLinearRing(koordinates),
                new LinearRing[0]);
        double d2 = localPolygon.getCentroid().getX();
        double d3 = localPolygon.getCentroid().getY();
        for (int j = 1; j < koordinates.length; j++) {
            LatLng taskas2 = new LatLng(koordinates[j].y, koordinates[j].x);
            double d4 = 111319.49079327358D * (taskas.longitude - d2) * Math.cos(3.141592653589793D * d3 / 180.0D);
            double d5 = (taskas.latitude - d3) * Math.toRadians(6378137.0D);
            double d6 = 111319.49079327358D * (taskas2.longitude - d2) * Math.cos(3.141592653589793D * d3 / 180.0D);
            d1 += d4 * ((taskas2.latitude - d3) * Math.toRadians(6378137.0D)) - d6 * d5;
            taskas = taskas2;
        }
        return 0.5D * Math.abs(d1);
    }

    public CameraUpdate showAllArea(List<LatLng> taskai, GoogleMap mMap) {
        CameraUpdate cu = null;
        LatLngBounds bounds = null;
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        if (taskai.size() > 0) {
            for (LatLng pos : taskai) {
                builder.include(pos);
                bounds = builder.build();
            }
        }
        int padding = 60;
        cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);

        return cu;
    }

    public boolean uztektinasAtstumas(List<LatLng> taskai) {
        if (taskai.size() > 2) {
            if (atstumas(taskai.get(0), taskai.get(taskai.size() - 1)) < 500)
                return true;
            else
                return false;
        } else
            return false;
    }

    public boolean isGpsOn(Context ctx) {
        LocationManager locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            return true;
        return false;
    }

    public double convertArea(double area, String unit) {
        if (unit.equals("kmĀ²"))
            return area * AREA_KM2;
        if (unit.equals("ftĀ²"))
            return area * AREA_FT2;
        if (unit.equals("ydĀ²"))
            return area * AREA_YD2;
        if (unit.equals("ha"))
            return area * AREA_HA;
        if (unit.equals("ac"))
            return area * AREA_ACRA;

        return area;
    }

    public double convertDistance(double distance, String unit) {
        if (unit.equals("km"))
            return distance * DIS_KM;

        if (unit.equals("ft"))
            return distance * DIS_FT;

        if (unit.equals("yd"))
            return distance * DIS_YD;

        return distance;
    }

/*    public CameraUpdate showAllAreas(List<Polygons> plotai, List<Marker> taskai, GoogleMap mMap) {
        CameraUpdate cu = null;
        LatLngBounds bounds = null;
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (int i = 0; i < plotai.size(); i++) {
            for (LatLng point : plotai.get(i).getPoints()) {
                builder.include(point);
                bounds = builder.build();

            }
        }
        if (taskai.size() > 0) {
            for (Marker b : taskai) {
                builder.include(b.getPosition());
                bounds = builder.build();
            }
        }

        int padding = 60;
        cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);

        return cu;
    }*/

    public List<LatLng> arAntLinijos(List<LatLng> taskai) {
        double paklaida = 0.25;
        double X1 = 0;
        double Y1 = 0;
        double X2 = 0;
        double Y2 = 0;
        double X3 = 0;
        double Y3 = 0;
        double atstumas = 0;
        for (int i = 0; i < taskai.size() - 2; i++) {
            atstumas = atstumasShp(taskai.get(i), taskai.get(i + 1));
            if (atstumas < 0.0025) {
                taskai.remove(i);
                i--;
            }
        }
        for (int i = 0; i < taskai.size() - 3; i++) {
            // formule: [ Ax * (By - Cy) + Bx * (Cy - Ay) + Cx * (Ay - By) ] / 2
            X1 = havX(taskai.get(i).latitude, taskai.get(i).longitude);
            X2 = havX(taskai.get(i + 1).latitude, taskai.get(i + 1).longitude);
            X3 = havX(taskai.get(i + 2).latitude, taskai.get(i + 2).longitude);

            Y1 = havY(taskai.get(i).latitude, taskai.get(i).longitude);
            Y2 = havY(taskai.get(i + 1).latitude, taskai.get(i + 1).longitude);
            Y3 = havY(taskai.get(i + 2).latitude, taskai.get(i + 2).longitude);

            double sk = X1 * (Y2 - Y3) + X2 * (Y3 - Y1) + X3 * (Y1 - Y2);
            sk = sk / 2;
            atstumas = atstumasShp(taskai.get(i), taskai.get(i + 2));

            if (sk > (-paklaida * atstumas) && sk < paklaida * atstumas) {
                taskai.remove(i + 1);
                i--;
            }
        }
        return taskai;
    }

    public double atstumasShp(LatLng taskas, LatLng taskas2) {
        // atstumas nuo vieno tasko iki kito
        final int R = 6371; // Zemes spindulys
        double distance = 0;
        Double latDistance = deg2rad(taskas2.latitude - taskas.latitude);
        Double lonDistance = deg2rad(taskas2.longitude - taskas.longitude);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(deg2rad(taskas.latitude))
                * Math.cos(deg2rad(taskas2.latitude)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        distance += R * c;
        // / double height = el1 - el2; //altitude
        // distance = Math.pow(distance, 2) + Math.pow(height, 2);
        // return Math.sqrt(distance);
        return distance;
    }

/*    *//**
     * Funkcijos atliekamas kai baigiamas importas
     * 
     * @param ctx
     *//*
    public void importDoneFunction(Context ctx) {
        for (Polygons a : Var.plotai) {
            if (a.getImportuoted()) {
                a.setImportoted(false);
                String coordinates = PointsFunctions.points2WKT(a.getPoints());
                ObjField field = new ObjField(a.getId(), a.getName(), a.getArea(), a.getArea(), a.getSeason(), 1, "", 0,
                        1, coordinates, -1);
                field.setUnique_number(Utils.generateRandom());
                Var.db.addField(field);
                
              //  Var.daugiakamp.setId(Var.db.getLastAddedFieldID());
            }
            a.setActive();
            Var.editMode = Var.FIELD;
            Var.look.skaiciavimaiGone();
            Var.intersectMode = false;
            Var.look.drawingButtonsBarHide();
            Var.look.startSave.setVisibility(View.VISIBLE);
            Fields.reloadFields();
        }
        Var.showDone = false;
        ((ActivityMap) ctx).supportInvalidateOptionsMenu();
    }*/

}
