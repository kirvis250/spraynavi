package lt.minvib.spraynavig.map;


import java.util.ArrayList;
import java.util.List;

import lt.minvib.spraynavig.DrawerActivity;
import lt.minvib.spraynavig.R;
import lt.minvib.spraynavig.fields.DatabaseFields;
import lt.minvib.spraynavig.fields.ObjField;
import lt.minvib.spraynavig.navilines.ActivitySaveNavigationLines;
import lt.minvib.spraynavig.spryfunction.Spray;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

public class ActivityMap extends DrawerActivity {

	
	

	private MenuItem changeMap;
	private MenuItem save;
	private MenuItem cancel;
	private SetMap setmap;
	private SharedPreferences prefs;


	public static List<Marker> marks;

	public static Polygon area;
	public static PolygonOptions opts;

	Functions func;
	Spray sprayer; 
	

	

	private Button createField;
	private Button spray;

	public static int mode;

	
	private LinearLayout scrollView;
	private LinearLayout areaDrawingTools;
	
	private ImageView undo;
	private ImageView cancell;


	public static final int MAP_MODE_DEFAULT = 0;
	public static final int MAP_MODE_MAKE_FIELD = 1;
	public static final int MAP_MODE_MAKE_POI = 2;
	public static final int MAP_MODE_SPRAY = 3;
	
	boolean isZoomDetermine = false;
int field_id = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		
		
		
		scrollView = (LinearLayout) findViewById(R.id.horizontalScrollView1);
		areaDrawingTools = (LinearLayout) findViewById(R.id.map_create);
		
		undo = (ImageView) findViewById(R.id.undo);
		undo.setOnClickListener(onUndo);
		
		cancell = (ImageView) findViewById(R.id.cancel_drawing);
		cancell.setOnClickListener(onCancel);
		
		areaDrawingTools.setVisibility(View.GONE);

		func = new Functions();

		createField = (Button) findViewById(R.id.newField);
		createField.setOnClickListener(onclickNewField);

		spray = (Button) findViewById(R.id.spray);
		spray.setOnClickListener(onSprayClickListener);
		
		Vr.seekBar1 = (SeekBar)  findViewById(R.id.seekBar1);
		Vr.seekBar1.setVisibility(View.GONE);

		setmap = new SetMap(this);
		Vr.map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

		setmap.setUpMap();
		
		
		Bundle b = getIntent().getExtras();
		
		if(b != null && b.getInt("field_id") > 0)
		{
			field_id = b.getInt("field_id");
			isZoomDetermine = true;
		} else isZoomDetermine = false;

		Vr.map.setOnMapLoadedCallback(new OnMapLoadedCallback() {	@Override public void onMapLoaded() {
			
			if(!isZoomDetermine){
				setmap.zoomToMyLocationFirstTime();
			MapFigureHand.addAllPolygonsToMap(ActivityMap.this);
			}
			else
			{
				DatabaseFields fields = new DatabaseFields(ActivityMap.this);
				
				ObjField field =  fields.getfield(field_id);
				field.getCoordinates();
				LatLng lats = field.getCoordinates().get(0);

				Vr.map.animateCamera( CameraUpdateFactory.newLatLngZoom(lats, 17F) , 1000, null);
				Vr.map.setOnCameraChangeListener(null);
				Vr.map.setOnMapLoadedCallback(null);
				
				MapFigureHand.addAllPolygonsToMap(ActivityMap.this);
				MapFigureHand.MapClickOriginal(lats, func, ActivityMap.this);		
			}
		
		}});
		Vr.map.setOnMapClickListener(orginalClick);
		Vr.map.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {
			
			@Override
			public void onMyLocationChange(Location location) {
				
				LatLng point= new LatLng(location.getLatitude(), location.getLongitude()); 
				if(mode == MAP_MODE_SPRAY)
					sprayer.onLocationChange(point);
			}
		});
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.map_meniu, menu);
		changeMap = (MenuItem) menu.findItem(R.id.action_change_map);
		cancel = (MenuItem) menu.findItem(R.id.action_cancel);
		save = (MenuItem) menu.findItem(R.id.action_save);

		return super.onCreateOptionsMenu(menu);
	}


	// On prepare options hide not needed
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		SaveNCancelVisibility(false, false);
		return super.onPrepareOptionsMenu(menu);
	}


	// Menu options on select listener
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {


		switch (item.getItemId()) {
		case R.id.action_change_map:
			Dailogs.setMapModeDialog(this);
			return true;

		case R.id.action_cancel:
			dialogDoCancel(this, false);
			return true;

		case R.id.action_save:


			if(mode == MAP_MODE_SPRAY ){
				Intent iaa = new Intent(ActivityMap.this, ActivitySaveNavigationLines.class);
				
				startActivityForResult(iaa, 22);
				
				return true;
			}
			
			if(area != null &&  area.getPoints().size() > 3){
				if(mode == MAP_MODE_MAKE_FIELD)
				{
					Vr.poly = area;
					Intent i = new Intent(this, ActivityEditField.class);
					startActivityForResult(i,10);
				}
			} else {
				Toast.makeText(this, "field needs more points", Toast.LENGTH_LONG).show();
			}


			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}



	private void  SaveNCancelVisibility(boolean isSave, boolean isCancel)
	{
		if(isSave){save.setVisible(true);}
		else	  {save.setVisible(false);}

		if(isCancel){cancel.setVisible(true);}
		else 		{cancel.setVisible(false);}
	}
	
	
	@Override
	protected void onDestroy() {
		
		if(area != null)
			area.remove();
		MapFigureHand.deleteMarkers(marks);
		
		scrollView.setVisibility(View.VISIBLE);
		areaDrawingTools.setVisibility(View.GONE);

		if(sprayer!= null){
			sprayer.endSpray();
			Vr.removeNaviLine();	
		}
		mode = 0;
		super.onDestroy();
	}


	@Override
	protected void onResume() {
		super.onResume();
		setmap.setUpMapIfNeeded();
	}

	@Override
	public void onBackPressed() {
		
		if(mode != MAP_MODE_DEFAULT)
			dialogDoCancel(this, true);
		else
			super.onBackPressed();
	}
	

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {


		if(requestCode == 10 && resultCode == RESULT_OK)
		{
			SaveNCancelVisibility(false, false);
			
			scrollView.setVisibility(View.VISIBLE);
			areaDrawingTools.setVisibility(View.GONE);

			MapFigureHand.deleteMarkers(marks);
			MapFigureHand.deletePoly(area);
			mode = 0;
			MapFigureHand.addAllPolygonsToMap(ActivityMap.this);
		}

	};


	OnClickListener onclickNewField = new OnClickListener() {

		@Override
		public void onClick(View v) {

			if(mode == MAP_MODE_MAKE_FIELD )
				return;

			mode = MAP_MODE_MAKE_FIELD;
			
			MapFigureHand.selectField(ActivityMap.this);
			
			SaveNCancelVisibility(true, false);
			
			scrollView.setVisibility(View.GONE);
			areaDrawingTools.setVisibility(View.VISIBLE);
			
			marks = new ArrayList<Marker>();
			opts = new PolygonOptions();
			area = null;
		}
	};




	OnClickListener onSprayClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			if(mode == MAP_MODE_SPRAY )
				return;

			mode = MAP_MODE_SPRAY;
			SaveNCancelVisibility(true, true);
			
			scrollView.setVisibility(View.GONE);
			areaDrawingTools.setVisibility(View.GONE);

			sprayer = new Spray(ActivityMap.this);
			marks = new ArrayList<Marker>();
			opts = new PolygonOptions();
			area = null;
			if(sprayer.initSpray() == -1)
			{
				scrollView.setVisibility(View.VISIBLE);
				areaDrawingTools.setVisibility(View.GONE);
				
				SaveNCancelVisibility(false, false);
				MapFigureHand.deleteMarkers(marks);

				if(sprayer!= null)
					sprayer.endSpray();
				mode = 0;
			}


		}
	};
	
	
	public  OnClickListener onUndo = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			MapFigureHand.removeLastAreaMarker(ActivityMap.this, opts);
		}
	}; 
	
	
	public  OnClickListener onCancel = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			dialogDoCancel(ActivityMap.this, false);
		}
	}; 
	

	OnMapClickListener orginalClick = new OnMapClickListener() {

		@Override
		public void onMapClick(LatLng point) {


			switch(mode){

			case MAP_MODE_DEFAULT:
				MapFigureHand.MapClickOriginal(point , func, ActivityMap.this );
				break;

			case MAP_MODE_MAKE_FIELD:
				MapFigureHand.mapClickCreatingAmap( point,  ActivityMap.this , opts);
				break;

			case MAP_MODE_SPRAY:
				sprayer.onMapClicked(point);
				break;

			}
		}
	};

	
	
	public  void dialogDoCancel(final Context ctx, final boolean doFinish){

		AlertDialog.Builder alert = new AlertDialog.Builder(ctx);

		alert.setTitle(getString(R.string.cancel_aproval)); 
		alert.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

			}
		});

		alert.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

					SaveNCancelVisibility(false, false);
					
					if(area != null)
						area.remove();
					MapFigureHand.deleteMarkers(marks);
					
					scrollView.setVisibility(View.VISIBLE);
					areaDrawingTools.setVisibility(View.GONE);

					if(sprayer!= null){
						sprayer.endSpray();
						Vr.removeNaviLine();	
					}
					mode = 0;
					
					if(doFinish)
						finish();

				// Do something with value!
			}
		});

		alert.show();
	}





}
