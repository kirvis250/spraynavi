package lt.minvib.spraynavig.map;

import java.util.List;

import lt.minvib.spraynavig.fields.ObjField;
import android.widget.SeekBar;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.Polyline;
import com.vividsolutions.jts.geom.LineSegment;


public class Vr {  // class of static variables like map

	
	public static GoogleMap map = null;  // initial value is null initiates in ActivityMap
	public static List<Polygon> polys = null;
	public static List<ObjField> polysF = null;
	public static int selected = -1;
	public static ObjField selectedF = null;
	
	public static LineSegment pp;
	
	public static Polygon poly = null;
	public static List<Polyline> navigationLines;
	
	public static LatLng p1;
	public static LatLng p2;
	
	public static SeekBar seekBar1;
	
	
	public static void removeNaviLine()
	{
		if(navigationLines != null)
		for(int i = 0; i < navigationLines.size(); i++)
		{
			navigationLines.get(i).remove();
		}
	}
	
	
	
	
	
}
