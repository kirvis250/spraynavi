package lt.minvib.spraynavig.map;

import lt.minvib.spraynavig.R;
import lt.minvib.spraynavig.fields.DatabaseFields;
import lt.minvib.spraynavig.fields.ObjField;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityEditField extends ActionBarActivity {
	
	
	
	private EditText tittle;
	
	private EditText fieldNumber;
	private EditText description;
	
	private Button save;
	
	private Functions  func; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add__field);
		
		 tittle = (EditText) findViewById(R.id.tittle_edit);
		
		 fieldNumber = (EditText) findViewById(R.id.Field_number_edit);
		 description = (EditText) findViewById(R.id.description_edit);
		
		 save = (Button) findViewById(R.id.save_button);
		 save.setOnClickListener(onSaveClick);
		 
		 func = new Functions();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_edit__field, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	
	OnClickListener onSaveClick = new  OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			if(tittle.getText().toString().length() < 1)
			{
				Toast.makeText(ActivityEditField.this, "Title not entered", Toast.LENGTH_LONG).show();
				return;
			}

			
			if(fieldNumber.getText().toString().length() < 1)
			{
				Toast.makeText(ActivityEditField.this, "field number not entered", Toast.LENGTH_LONG).show();
				return;
			}
			
			String title = tittle.getText().toString();
			double area = func.areaCalc(Vr.poly.getPoints());
			String dssescription = description.getEditableText().toString();
			DatabaseFields dbc = new DatabaseFields(ActivityEditField.this);
			String number = fieldNumber.getEditableText().toString();
			ObjField field = new ObjField(0,title, area, 0, Vr.poly.getPoints(), System.currentTimeMillis(), dssescription, number);
			
			dbc.addField(field);
			
			setResult(RESULT_OK);
			finish();
		}
	};
	
	
	
}
