package lt.minvib.spraynavig.map;

import lt.minvib.spraynavig.Cons;
import lt.minvib.spraynavig.R;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

public class SetMap {
    private Context ctx;
  //  private DatabasePoi dbP;
    
    private SharedPreferences prefs;
    
    public SetMap(Context ctx) {
        super();
        this.ctx = ctx;
        prefs = ctx.getSharedPreferences(Cons.PREF_SETTINGS, Context.MODE_PRIVATE);
     //   dbP = new DatabasePoi(ctx);
    }
    
    /** Tikrinimas ar map'ai prieinami ir setUp'inama map'o rodym�� **/
    public void setUpMapIfNeeded() {
        if (Vr.map == null) {
            Vr.map = ((SupportMapFragment) ((ActivityMap)ctx).getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
           // Vr.map = ((MapFragment) ((ActivityMap)ctx).getFragmentManager().findFragmentById( R.id.map)).getMap();
            
            if (Vr.map != null)
                setUpMap();
        }
    }
    
    
    

    public void setUpMap() {
        // Var.mMap.setMyLocationEnabled(true);
    	
    	int type =  prefs.getInt(Cons.PREF_LAST_MAP_TYPE, GoogleMap.MAP_TYPE_HYBRID);
    	
        Vr.map.setMapType(type);
        
        prefs.edit().putInt(Cons.PREF_LAST_MAP_TYPE, type).commit();
        
        Vr.map.setMyLocationEnabled(true);
        
        
        // Var.mMap.getUiSettings().setMyLocationButtonEnabled(true);
/*        Var.mMap.setInfoWindowAdapter(new InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
            	View view = ((ActivityMap)ctx).getLayoutInflater().inflate(R.layout.empty, null);
            	if(marker.getTitle().equals(String.valueOf(Cons.POI))){
	                View v = ((ActivityMap)ctx).getLayoutInflater().inflate(R.layout.map_marker_window, null);
	                TextView title = (TextView) v.findViewById(R.id.title);
	                Var.clickedPoi = marker;
	                
	                title.setText(dbP.getPoiTitle(Integer.valueOf((marker.getSnippet()))));
	                view = v;
            	}
                
//                else if (Var.updater != null)
//            		view = Var.updater.infoWindowAdapter(marker);
                
                return view;
            }
        });*/

//        Var.mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
//
//            @Override
//            public void onInfoWindowClick(Marker arg0) {
//            	//kviečia tik POI langą ir tik kada būna nebraižomas laukas
//            	if(arg0.getTitle().equals(String.valueOf(Cons.POI)) && Var.area_done )
//            		PoiDialogs.poiClick(ctx, Var.clickedPoi);            	
//            }
//
//        });
    }

    /** Pagal ekrano orientacij� nustatoma zoom control vieta **/
    public void zoomControls(String side) {
        SupportMapFragment mapFragment = (SupportMapFragment) ((ActivityMap)ctx).getSupportFragmentManager().findFragmentById(
                R.id.map);
        View zoomControls = mapFragment.getView().findViewById(0x1);
        if (zoomControls != null && zoomControls.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) zoomControls.getLayoutParams();
            if (side.equals("RIGHT")) {
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            } else {
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            }
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, ctx.getResources()
                    .getDisplayMetrics());
            params.setMargins(margin, margin, margin, margin * 6);
        }
    }
    
    
    
    public void zoomToMyLocationFirstTime()
    {
		LocationManager mlocManager = (LocationManager) ctx
				.getSystemService(Context.LOCATION_SERVICE);
		Location currentGeoLocation = mlocManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

		if(currentGeoLocation == null)
			currentGeoLocation = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);


		if(currentGeoLocation == null)
			currentGeoLocation = mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

		if(currentGeoLocation == null)
			currentGeoLocation = Vr.map.getMyLocation();

		if(currentGeoLocation != null){

			LatLng lats = new LatLng(currentGeoLocation.getLatitude(), currentGeoLocation.getLongitude()); 

			Vr.map.animateCamera( CameraUpdateFactory.newLatLngZoom(lats, 17F) , 1000, null);
			Vr.map.setOnCameraChangeListener(null);
			Vr.map.setOnMapLoadedCallback(null);

		} else {
			Log.i("Fucking Bullshit", "ghdfh");
		}
    }
    
    
    
}
