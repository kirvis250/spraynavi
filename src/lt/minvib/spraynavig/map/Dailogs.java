package lt.minvib.spraynavig.map;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.GoogleMap;

import lt.minvib.spraynavig.Cons;
import lt.minvib.spraynavig.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

public class Dailogs {



	public static void setMapModeDialog(final Context ctx)
	{
		if(Vr.map != null){
			
			final SharedPreferences shared = ctx.getSharedPreferences(Cons.PREF_SETTINGS, Context.MODE_PRIVATE);

			final List<ChooseModeItem> items = new ArrayList<ChooseModeItem>();
			items.add(new ChooseModeItem(ctx.getString(R.string.map_type_hybrid), R.drawable.ic_action_map));
			items.add(new ChooseModeItem(ctx.getString(R.string.map_type_normal), R.drawable.ic_action_map));  
			items.add(new ChooseModeItem(ctx.getString(R.string.map_type_satellite), R.drawable.ic_action_map));
			items.add(new ChooseModeItem(ctx.getString(R.string.map_type_terrain), R.drawable.ic_action_map));
			items.add(new ChooseModeItem(ctx.getString(R.string.map_type_none), R.drawable.ic_action_map));  

			ListAdapter adapter = new ArrayAdapter<ChooseModeItem>(ctx, android.R.layout.select_dialog_item,
					android.R.id.text1, items) {
				public View getView(int position, View convertView, ViewGroup parent) {
					View v = super.getView(position, convertView, parent);
					TextView tv = (TextView) v.findViewById(android.R.id.text1);
					tv.setCompoundDrawablesWithIntrinsicBounds(items.get(position).icon, 0, 0, 0);
					int dp5 = (int) (5 * ctx.getResources().getDisplayMetrics().density + 0.5f);
					tv.setCompoundDrawablePadding(dp5);
					return v;
				}
			};

			new AlertDialog.Builder(ctx).setTitle(ctx.getString(R.string.action_Change_Map_type))
			.setAdapter(adapter, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case 0:  // if images is taken by camera
						Vr.map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
						shared.edit().putInt(Cons.PREF_LAST_MAP_TYPE, GoogleMap.MAP_TYPE_HYBRID).commit();
						break;

					case 1:	
						Vr.map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
						shared.edit().putInt(Cons.PREF_LAST_MAP_TYPE, GoogleMap.MAP_TYPE_NORMAL).commit();
						break;

					case 2: 
						Vr.map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
						shared.edit().putInt(Cons.PREF_LAST_MAP_TYPE, GoogleMap.MAP_TYPE_SATELLITE).commit();
						break;

					case 3:  
						Vr.map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
						shared.edit().putInt(Cons.PREF_LAST_MAP_TYPE, GoogleMap.MAP_TYPE_TERRAIN).commit();
						break;

					case 4: 
						Vr.map.setMapType(GoogleMap.MAP_TYPE_NONE);
						shared.edit().putInt(Cons.PREF_LAST_MAP_TYPE, GoogleMap.MAP_TYPE_NONE).commit();
						
						
						break;

					}
				}
			}).show();

		}
	}

}
