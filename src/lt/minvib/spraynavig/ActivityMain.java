package lt.minvib.spraynavig;

import java.util.ArrayList;
import java.util.List;

import lt.minvib.spraynavig.fields.DatabaseFields;
import lt.minvib.spraynavig.fields.ObjField;
import lt.minvib.spraynavig.map.ActivityMap;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ActivityMain extends DrawerActivity {


	List<String> values;
	
	ListView listView1;
	
	ArrayAdapter<String> adapter;
	
	DatabaseFields dbf;
	
	List<ObjField> fields;

	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		
		
		listView1 = (ListView) findViewById(R.id.listView1);
		
		dbf = new DatabaseFields(this);
		fields = dbf.getAllFields();
		
		if(fields != null && fields.size() > 0){
		
		values = new ArrayList<String>();
		adapter = new ArrayAdapter<String>(this, 
				android.R.layout.simple_list_item_1, values);
		
		listView1.setAdapter(adapter);
		
		listView1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				Intent i = new Intent(ActivityMain.this, ActivityMap.class);
				i.putExtra("field_id", fields.get(position).getId());
				startActivity(i);
			}
		});
		
			for(int i = 0 ; i < fields.size(); i++)
			{
				values.add(fields.get(i).getTitle());
			}
		}
	}
	
	
	
	
	
	
	
	

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.second_activty, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle action bar item clicks here. The action bar will
//		// automatically handle clicks on the Home/Up button, so long
//		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
}
