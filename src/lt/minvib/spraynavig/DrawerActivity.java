package lt.minvib.spraynavig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lt.minvib.spraynavig.map.ActivityMap;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.service.textservice.SpellCheckerService.Session;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

public class DrawerActivity extends ActionBarActivity {

	private DrawerLayout mDrawerLayout;
	private static ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private String[] categoryTitles;
	protected FrameLayout actContent;
	int parentLayout;
	private boolean dispatch = false;
	private SharedPreferences sharedPrefs;
	private Session session;
	protected String userName = "";
	
	private Intent intent;

	@Override
	public void setContentView(final int resLayoutID) {
		LinearLayout fullLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.fragment_navigation_drawer, null);
		actContent = (FrameLayout) fullLayout.findViewById(R.id.content_frame);
		super.setContentView(fullLayout);	
		getLayoutInflater().inflate(resLayoutID, actContent, true);
		mDrawerLayout = (DrawerLayout) fullLayout.findViewById(R.id.drawer_layout);
		ini();
		createSideList();
	}
	
	protected void onCreate(Bundle savedInstanceState, int resLayoutID) {
		super.onCreate(savedInstanceState);; 
	}

	@Override
	public void onStart() {
		super.onStart();
		
	}

	@Override
	protected void onStop() {
		super.onStop();
		
	}
	
	@Override
	protected void onResume(){
		super.onResume();
	
	}

	/**
	 * Inicializuojami reikalingi kintamieji
	 */
	private void ini() {

		RelativeLayout userSettings = (RelativeLayout) findViewById(R.id.user_settings);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		
		

		    toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
		    setSupportActionBar(toolbar);

		    mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,toolbar ,  R.string.app_name, R.string.app_name);


		    // Set the drawer toggle as the DrawerListener 
		    mDrawerLayout.setDrawerListener(mDrawerToggle);

		    getSupportActionBar().setDisplayHomeAsUpEnabled(true); 
		    getSupportActionBar().setHomeButtonEnabled(true); 

		    sharedPrefs = getSharedPreferences("settings", 0);
		
	}
	
	private void createSideList()
	{
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		
	
	//	categoryTitles = getResources().getStringArray(R.array.menuCategories);
		//String[] categoryIcons = getResources().getStringArray(
			//	R.array.menuIcons);
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);

		List<Map<String, String>> listMap = new ArrayList<Map<String, String>>();
		Map<String, String> map;
		
		
			map = new HashMap<String, String>();
			int img_id = getResources().getIdentifier("",
					"drawable", getPackageName());
			map.put("icon", String.valueOf(R.drawable.ic_action_map));
			map.put("title", "�em�lapis");

			listMap.add(map);
			
			
			
		

		CustomAdapter adapter = new CustomAdapter(this, listMap,
				R.layout.drawer_list_item, new String[] { "title", "icon" },
				new int[] { R.id.drawer_item_name, R.id.imageView1 }  ) {

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View view = super.getView(position, convertView, parent);
				
				//TextView title = (TextView) view.findViewById(R.id.categoryTitle);
			//	TextView notifications = (TextView) view.findViewById(R.id.notifications);
			//	if (Utils.getString(title).equals(getString(R.string.m_notifications))){
			//		notifications.setVisibility(View.VISIBLE);
				//	notifications.setText(String.valueOf(dbN.getActiveCount()));
			//	}
			//	else
			//		notifications.setVisibility(View.GONE);
				return view;
			}
		};

		mDrawerList.setAdapter(adapter); 
		
		
		mDrawerList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				switch(position){
				
				case 0:

					callDashboard();
					intent = new Intent(DrawerActivity.this, ActivityMap.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					overridePendingTransition(R.anim.activity_from_flip_transition, R.anim.activity_to_flip_transition);
					finish();

					break;
				
				}
			}
			
		});

	}
	
	
	
	public void callDashboard()
	{
		Intent main = new Intent(DrawerActivity.this, ActivityMain.class);
			main.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(main);
	}

		

	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	/**
	 * Suformuoja meniu kategorijų list'ą su ikonom
	 */




}