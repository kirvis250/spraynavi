package lt.minvib.spraynavig.fields;

import java.util.List;

import com.google.android.gms.maps.model.LatLng;

public class ObjField {

	private int id;
	private String title;
	private double area;
	private int group;
	private List<LatLng> coordinates;
	private long created_date;
	private String description;
	private String fieldNumber;
	
	
	public ObjField(int id,String title, double area, int group,
			List<LatLng> coordinates, long created_date, String description, String fieldNumber   ) {
		
		 this.id = id;
		 this.title= title;
		 this.area= area;
		 this.group= group;
		 this.coordinates= coordinates;
		 this.created_date= created_date;
		 this.description= description;
		 this.fieldNumber= fieldNumber;
	}
	
	public String getTitle() {return title;}
	public void setTitle(String title) {this.title = title;}
	public double getArea() {return area;}
	public void setArea(double area) {this.area = area;}
	public int getGroup() {return group;}
	public void setGroup(int group) {this.group = group;}
	public List<LatLng> getCoordinates() {return coordinates;}
	public void setCoordinates(List<LatLng> coordinates) {this.coordinates = coordinates;}
	public long getCreated_date() {return created_date;}
	public void setCreated_date(long created_date) {this.created_date = created_date;}
	public String getDescription() {return description;}
	public void setDescription(String description) {this.description = description;}
	public String getFieldNumber() {return fieldNumber;}
	public void setFieldNumber(String fieldNumber) {this.fieldNumber = fieldNumber;}
	public int getId() {return id;}
	public void setId(int id) {this.id = id;}	
	
	
}
