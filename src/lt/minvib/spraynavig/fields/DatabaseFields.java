package lt.minvib.spraynavig.fields;

import java.util.ArrayList;
import java.util.List;

import lt.minvib.spraynavig.DB;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.android.gms.maps.model.LatLng;

public class DatabaseFields {

	private Context ctx;


	public DatabaseFields( Context  ctx) {
		this.ctx = ctx;
	}
	
	
	
	public int addField(ObjField obj)
	{
		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();

		ContentValues insertValues = new ContentValues();

		insertValues.put(DB.KEY_NAME, obj.getTitle());
		insertValues.put(DB.KEY_AREA_1, obj.getArea());
		insertValues.put(DB.KEY_CREATION_DATE , obj.getCreated_date());
		insertValues.put(DB.KEY_GROUP, 0);
		insertValues.put(DB.KEY_FIELD_NUMBER, obj.getFieldNumber());

		insertValues.put(DB.KEY_COORDINATES, LatLngToString(obj.getCoordinates()));
		insertValues.put(DB.KEY_DESCRIPTION, obj.getDescription());

		insertValues.put(DB.KEY_UNIQUE_NUMBER, 0);
		insertValues.put(DB.KEY_DELETED, 0);

		long newId = db.insert(DB.TABLE_FIELD, null, insertValues);
 
		return (int)newId;
		
	}
	
	
/*	 + KEY_ID + " INTEGER PRIMARY KEY," 
	 + KEY_NAME + " VARCHAR,"
	 + KEY_AREA_1 + " REAL," 
	 + KEY_CREATION_DATE + " INTEGER," 
	 + KEY_GROUP + " INTEGER," 
	 + KEY_FIELD_NUMBER + " VARCHAR," 
	 + KEY_COORDINATES + " VARCHAR, " 
	 + KEY_DESCRIPTION + " VARCHAR,"
	 + KEY_UNIQUE_NUMBER +" INTEGER, "
	 + KEY_DELETED + " INTEGER DEFAULT 0)";*/
	
	
	
	
	
	public List<ObjField> getAllFields()
	{
		
		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		Cursor cursor = null;

		List<ObjField> fields = new ArrayList<ObjField>();

		 cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_FIELD + " ORDER BY "+ DB.KEY_ID +" DESC ;", null);


		cursor.moveToFirst();
		while (cursor.isAfterLast()== false) {

			
			
/*			
			 + KEY_ID + " INTEGER PRIMARY KEY," 
			 + KEY_NAME + " VARCHAR,"
			 + KEY_AREA_1 + " REAL," 
			 + KEY_CREATION_DATE + " INTEGER," 
			 + KEY_GROUP + " INTEGER," 
			 + KEY_FIELD_NUMBER + " VARCHAR," 
			 + KEY_COORDINATES + " VARCHAR, " 
			 + KEY_UNIQUE_NUMBER +" INTEGER, "
			 + KEY_DELETED + " INTEGER DEFAULT 0)";*/
			
			int id = cursor.getInt(0);
			
			String name = cursor.getString(1);
			double area  = cursor.getDouble(2);
			long created_date = cursor.getLong(3);
			int group = cursor.getInt(4);
			String  fieldNumber = cursor.getString(5);
			List<LatLng> coordinates = StrToLatLng(cursor.getString(6));
			String description = cursor.getString(7);
			
			ObjField field = new ObjField(id,name, area, group, coordinates, created_date, description, fieldNumber);
		
			fields.add(field);	

			cursor.moveToNext();
		}
		cursor.close();
		return fields;
	
	}
	
	
	
	public ObjField getfield(int id)
	{
		
		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		Cursor cursor = null;

		ObjField field = null;

		 cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_FIELD + " WHERE "+ DB.KEY_ID +"=" + id + " ;", null);

		cursor.moveToFirst();
		if(cursor.isAfterLast()== false) {

			
			int ids = cursor.getInt(0);
			
			String name = cursor.getString(1);
			double area  = cursor.getDouble(2);
			long created_date = cursor.getLong(3);
			int group = cursor.getInt(4);
			String  fieldNumber = cursor.getString(5);
			List<LatLng> coordinates = StrToLatLng(cursor.getString(6));
			String description = cursor.getString(7);
			
			field = new ObjField(ids,name, area, group, coordinates, created_date, description, fieldNumber);
		}
		cursor.close();
		return field;
		
		
		
		
	}
	
	
	public static String LatLngToString( List<LatLng> list)
	{
		String returnable = "";
		
		
		for(int i = 0; i < list.size(); i++)
		{
			LatLng thi = list.get(i); 
			returnable  = returnable + thi.latitude + "&"+ thi.longitude + "&";
		}
		
		returnable = returnable.substring(0, returnable.length()-1);
		return returnable;
	}
	
	public static List<LatLng> StrToLatLng(String parsable)
	{
		String [] arr = parsable.split("&");
		
		List<LatLng> list = new ArrayList<LatLng>();
		
		for(int i = 0; i < arr.length -1 ; i=i+2)
		{
			LatLng lat = new LatLng( Double.valueOf( arr[i]),  Double.valueOf( arr[i+1]));
			list.add(lat);
		}
		
		return list;
	}
	
	
	
	
	
	
/*	 				 + KEY_ID + " INTEGER PRIMARY KEY," 
				 + KEY_NAME + " VARCHAR,"
				 + KEY_AREA_1 + " REAL," 
				 + KEY_AREA_FROM_MAP + " REAL," 
				 + KEY_GROUP + " INTEGER," 
				 + KEY_FIELD_NUMBER + " VARCHAR," 
				 + KEY_COORDINATES + " VARCHAR, " 
				 + KEY_UNIQUE_NUMBER +" INTEGER, "
				 + KEY_DELETED + " INTEGER DEFAULT 0)";*/

	 
	/** paima klausimą pagal jo telefono pusės id 
	 * 
	 * @param id - telefono pusės id
	 * @return - obiektas
	 *//*
	public ObjSos getSosById(int id)
	{
		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		Cursor cursor = null;
		ObjSos problem = null;


		cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_SOS + " WHERE "
				+ DB.SOS_ID + " = " + id + " ;", null);


		if(cursor.moveToFirst()) {

			int iid  = cursor.getInt(0);
			int farmeId = cursor.getInt(1);
			String str = cursor.getString(2);
			int cate = cursor.getInt(3);
			int diag = cursor.getInt(4);
			int prid = cursor.getInt(5);
			String comment =  cursor.getString(6);

			int fieldId = cursor.getInt(7);
			int ans = cursor.getInt(8);
			int rew = cursor.getInt(9);
			String urls = cursor.getString(10);
			String IntUrls = cursor.getString(11);
			String names  = cursor.getString(12);
			Long date = cursor.getLong(13);

			int objId = cursor.getInt(14);
			
			int isProblems = cursor.getInt(15);
			boolean deleted  = cursor.getInt(16) == 1;

			problem  = new ObjSos( iid, farmeId, str, cate, diag, prid, comment,
					ans > 0, rew > 0, fieldId, resolveUrls(urls), resolveUrls(IntUrls), resolveUrls(names), date, objId,isProblems , deleted);

		}
		cursor.close();
		return problem;
	}



	*//** paima klausimą pagal jo serverio pusės id
	 * 
	 * @param id - serverio pusės id
	 * @return - obiektas
	 *//*
	public ObjSos getSosByObjId( int id)
	{
		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		Cursor cursor = null;
		ObjSos problem = null;


		cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_SOS + " WHERE "
				+ DB.SOS_OBJ_ID + " = " + id + " ;", null);


		if(cursor.moveToFirst()) {

			int iid  = cursor.getInt(0);
			int farmeId = cursor.getInt(1);
			String str = cursor.getString(2);
			int cate = cursor.getInt(3);
			int diag = cursor.getInt(4);
			int prid = cursor.getInt(5);
			String comment =  cursor.getString(6);

			int fieldId = cursor.getInt(7);
			int ans = cursor.getInt(8);
			int rew = cursor.getInt(9);
			String urls = cursor.getString(10);
			String IntUrls = cursor.getString(11);
			String names  = cursor.getString(12);
			Long date = cursor.getLong(13);

			int objId = cursor.getInt(14);
			
			
			int isProblems = cursor.getInt(15);
			boolean deleted  = cursor.getInt(16) == 1;

			problem  = new ObjSos( iid, farmeId, str, cate, diag, prid, comment,
					ans > 0, rew > 0, fieldId, resolveUrls(urls), resolveUrls(IntUrls), resolveUrls(names), date, objId,isProblems , deleted);
		}
		cursor.close();
		return problem;
	}


	*//** gauna visas problemas
	 * 
	 * @param farmerId
	 * @return
	 *//*
	public List<ObjSos> getSOSAll( )
	{

		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		Cursor cursor = null;
		ObjSos problem;

		List<ObjSos> problems = new ArrayList<ObjSos>();

		            cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_SOS + " WHERE "
                    + DB.SOS_FARMER_ID + " = " + farmerId + " ORDER BY "+ DB.SOS_DATE +" DESC ;", null);

		cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_SOS + " ORDER BY "+ DB.SOS_DATE +" DESC ;", null);

		cursor.moveToFirst();
		while (cursor.isAfterLast()== false) {

			int iid  = cursor.getInt(0);
			int farmeId = cursor.getInt(1);
			String str = cursor.getString(2);
			int cate = cursor.getInt(3);
			int diag = cursor.getInt(4);
			int prid = cursor.getInt(5);
			String comment =  cursor.getString(6);

			int fieldId = cursor.getInt(7);
			int ans = cursor.getInt(8);
			int rew = cursor.getInt(9);
			String urls = cursor.getString(10);
			String IntUrls = cursor.getString(11); 
			String names = cursor.getString(12);  
			Long date = cursor.getLong(13);

			int objId = cursor.getInt(14);


			int isProblems = cursor.getInt(15);
			boolean deleted  = cursor.getInt(16) == 1;

			problem  = new ObjSos( iid, farmeId, str, cate, diag, prid, comment,
					ans > 0, rew > 0, fieldId, resolveUrls(urls), resolveUrls(IntUrls), resolveUrls(names), date, objId,isProblems , deleted);

			problems.add(problem);	

			cursor.moveToNext();
		}
		cursor.close();
		return problems;
	}

	
	*//** graÅ¾ina true, jei yra neatsakytÅ³ problemÅ³
	 *  
	 * @return - true arba false
	 *//*
	public boolean isUnansweredProblemsExist()
	{

		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		Cursor cursor = null;

		cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_SOS + " WHERE "
				+ DB.SOS_ANSWERED + " = -1;", null);

		if(cursor.moveToFirst()) {
				cursor.close();
				return true;
		} else {
			cursor.close();
			return false;
		}
	}




	*//** atnaujina problemÄ… (atskymas ÄÆ problemÄ…)
	 * 
	 * @param id
	 * @param problem
	 *//*
	public void updateProblemByProblemId(int id, ObjSos problem)
	{
		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();

		ContentValues insertValues = new ContentValues();

		insertValues.put(DB.SOS_FARMER_ID, problem.getFarmerId());

		insertValues.put(DB.SOS_STR, problem.getProblem());
		insertValues.put(DB.SOS_CATEGORY, problem.getCategoryId());
		insertValues.put(DB.SOS_DIAGNOSIS, problem.getDiagnosisId());
		insertValues.put(DB.SOS_PRODUCT_ID, problem.getProductId());
		insertValues.put(DB.SOS_COMMENT, problem.getComment());
		insertValues.put(DB.SOS_FIELD_ID, problem.getFieldId());


		if(problem.isAnswered())	insertValues.put(DB.SOS_ANSWERED, 1 );
		else 						insertValues.put(DB.SOS_ANSWERED, -1 );

		if(problem.isReviewed())	insertValues.put(DB.SOS_REVIEWD, 1 );
		else 						insertValues.put(DB.SOS_REVIEWD, -1 );


		insertValues.put(DB.SOS_IMGURLS, urlListToString(problem.getImageUrls()));
		insertValues.put(DB.SOS_SERVER_IMG_URLS, urlListToString(problem.getserverImageUrls()));
		insertValues.put(DB.SOS_IMAGE_NAMES, urlListToString(problem.getImageNames()));
		insertValues.put(DB.SOS_DATE, problem.getDate());

		insertValues.put(DB.SOS_OBJ_ID, problem.getObjId());
		insertValues.put(DB.SOS_THERE_IS_NEW_ANSWERS, problem.getThereIsNewProblems());
		insertValues.put(DB.SOS_DELETE, problem.isDeleted());

		db.update(DB.TABLE_SOS, insertValues, DB.SOS_ID + "=" + id, null);
	}
	
	
	
	*//** pažymi problemą turinčią, arba neturinčią naujų atsakymų
	 * 
	 * @param id - problemos vidinis id ( Jeigu  < 1, atnaujins visus įraƵs  )
	 * @param chages - pasikeitimų buvimas (0 - nėra, 1 - yra)
	 *//*
	public void updateProblemIsThereAreChanges(int id, int chages)
	{
		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		ContentValues insertValues = new ContentValues();
		insertValues.put(DB.SOS_THERE_IS_NEW_ANSWERS, chages);

		if(id > 0)
			db.update(DB.TABLE_SOS, insertValues, DB.SOS_ID + "=" + id, null);
		else 
			db.update(DB.TABLE_SOS, insertValues, null, null);
	}




	*//** prideda problemÄ…
	 * 
	 * @param problem
	 * @return long new autoincremented id, -1 if error
	 *//*
	public long addSos( ObjSos problem )
	{		
		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();


		ContentValues insertValues = new ContentValues();

		insertValues.put(DB.SOS_FARMER_ID, problem.getFarmerId());

		insertValues.put(DB.SOS_STR, problem.getProblem());
		insertValues.put(DB.SOS_CATEGORY, problem.getCategoryId());
		insertValues.put(DB.SOS_DIAGNOSIS, problem.getDiagnosisId());
		insertValues.put(DB.SOS_PRODUCT_ID, problem.getProductId());
		insertValues.put(DB.SOS_COMMENT, problem.getComment());
		insertValues.put(DB.SOS_FIELD_ID, problem.getFieldId());


		if(problem.isAnswered())	insertValues.put(DB.SOS_ANSWERED, 1 );
		else 						insertValues.put(DB.SOS_ANSWERED, -1 );

		if(problem.isReviewed())	insertValues.put(DB.SOS_REVIEWD, 1 );
		else 						insertValues.put(DB.SOS_REVIEWD, -1 );

		insertValues.put(DB.SOS_IMGURLS, urlListToString(problem.getImageUrls()));
		insertValues.put(DB.SOS_SERVER_IMG_URLS, urlListToString(problem.getserverImageUrls()));
		insertValues.put(DB.SOS_IMAGE_NAMES, urlListToString(problem.getImageNames()));
		insertValues.put(DB.SOS_DATE, problem.getDate());

		insertValues.put(DB.SOS_OBJ_ID, problem.getObjId());
		
		insertValues.put(DB.SOS_THERE_IS_NEW_ANSWERS, problem.getThereIsNewProblems());
		insertValues.put(DB.SOS_DELETE, problem.isDeleted());

		long newId = db.insert(DB.TABLE_SOS, null, insertValues);
 
		return newId;
	}


	*//** iÅ�trinti problemÄ… pagal id
	 * 
	 * @param id
	 *//*
	public void deleteProblem(int id)
	{
		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		
		//ObjSos sos =  getSosById(id);
		
		DatabaseAnswersNew anss = new DatabaseAnswersNew(ctx);
		
		anss.deleteAnswersByProblemId(id);
		
		db.delete(DB.TABLE_SOS, DB.SOS_ID + "='" + id + "'", null);
	}


	*//** parsina url'us iŠurl'ų stringo
	 * 
	 * @param url - url stringas
	 * @return - url stringų listas
	 *//*
	private List<String> resolveUrls(String url)
	{
		if(url != null && url.length()>0)
		{
			String [] words = url.split(" @ ");
			return Arrays.asList(words);
		} else {
			return null;
		}
	}
	
	*//** sudeda url'us į vieną stringą
	 * 
	 * @param urls - adresų stringų listas
	 * @return - adresų stringas
	 *//*
	private String urlListToString(List<String> urls)
	{
		String returnable = "";
		if(urls != null && urls.size() > 0)
			for(String a : urls)
			{
				returnable = returnable + a +" @ ";
			}

		return returnable;
	}

	
	*//** iƫrapƴo lauko vardą 
	 * 
	 * @param fieldid - lauko id
	 * @param ct - kontekstas
	 * @return - lauko vardas arba R.string.field_is_unknown, jeigu laukas nerastas
	 *//*
	public static String resolveFieldName( int fieldid, Context ct)
	{
		if(fieldid > -1){
			DatabaseFields fields = new DatabaseFields(ct);

			ObjField field = fields.getFieldById(fieldid);

			if(field.getName()== null)
			{
				return ct.getString(R.string.field_is_unknown);
			}
			return field.getName() +", "+ field.getSize() + " ha";
		}
		return "";
	}
	
	
	*//** iƫrapƴo lauką pagal jo id - reikėtų kažkur Ʃtą perkelt
	 * 
	 * @param fieldid - lauko id
	 * @param ct	- kontekstas
	 * @return	- Lauko objektas
	 *//* 
	public static ObjField resolveField( int fieldid, Context ct)
	{
		if(fieldid > -1){
			DatabaseFields fields = new DatabaseFields(ct);

			ObjField field = fields.getFieldById(fieldid);

			if(field.getName()== null)
			{
				return null;  
			}
			return field;
		}
		return null;
	}

	*//** Atnaujint arba neatnaujint problemos (padauodama nauja gauta iŠserverio problema)
	 * 
	 * @param newGet - nauja problema
	 * @return - problema kuri buvo atnaujinta, arba įraƹta į duombazę
	 *//*
	public ObjSos addSosOrChange(ObjSos newGet)
	{
				ObjSos newOne  = newGet;
				ObjSos sosOrigin = getSosByObjId(newOne.getObjId());
				if(sosOrigin != null)
				{
					ObjSos updatable = sosOrigin;
					updatable.setServerImageUrls(sosOrigin.getserverImageUrls());
					
					// Jeigu telefone pažymėta, kaip perskaityta tai neatnaujina - ATEITY GALI KEISTIS...
					if(!sosOrigin.isReviewed())
						updatable.setReviewed(newOne.isReviewed());
					
					updatable.setDate(newOne.getDate());
					updateProblemByProblemId(updatable.getId(), updatable);
					updatable.isNew = false;
					return updatable;
					
				} else {
					newOne.setId((int)addSos(newOne));
					newOne.isNew = true;
					return newOne;
				}		
	}
	
	
	
	*//** checks if there is not sended questions to server
	 * 
	 * @param ctx - app context
	 * @return	- List of unsent items
	 *//*
	public List<ObjSos> getNotSentItems()
	{

		SQLiteDatabase db = DB.getDB(ctx).getWritableDatabase();
		Cursor cursor = null;
		ObjSos problem;

		List<ObjSos> problems = new ArrayList<ObjSos>();

		//      cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_PROBLEMS_FOR_SOLVING + " WHERE "
		//      + DB.PROBLEM_FARMER_ID + " = " + farmerId + " ORDER BY "+ DB.PROBLEM_DATE +" DESC ;", null);

		cursor = db.rawQuery("SELECT * FROM " + DB.TABLE_SOS + " WHERE "
				+ DB.SOS_OBJ_ID + " = 0 "+  " ORDER BY "+ DB.SOS_DATE +" DESC ;", null);

		cursor.moveToFirst();
		while (cursor.isAfterLast()== false) {

			int iid  = cursor.getInt(0);
			int farmeId = cursor.getInt(1);
			String str = cursor.getString(2);
			int cate = cursor.getInt(3);
			int diag = cursor.getInt(4);
			int prid = cursor.getInt(5);
			String comment =  cursor.getString(6);

			int fieldId = cursor.getInt(7);
			int ans = cursor.getInt(8);
			int rew = cursor.getInt(9);
			String urls = cursor.getString(10);
			String IntUrls = cursor.getString(11);
			String names = cursor.getString(12);
			Long date = cursor.getLong(13);

			int objId = cursor.getInt(14);

			problem = new ObjSos(iid, farmeId, str, cate, diag, prid, comment, ans >0, rew >0, fieldId);

			int isProblems = cursor.getInt(15);
			boolean deleted  = cursor.getInt(16) == 1;

			problem  = new ObjSos( iid, farmeId, str, cate, diag, prid, comment,
					ans > 0, rew > 0, fieldId, resolveUrls(urls), resolveUrls(IntUrls), resolveUrls(names), date, objId,isProblems , deleted);


			if(!problem.isAnswered())
				problems.add(problem);	

			cursor.moveToNext();
		}
		cursor.close();
		return problems;
	}*/


}
