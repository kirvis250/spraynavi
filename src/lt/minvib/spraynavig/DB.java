package lt.minvib.spraynavig;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DB extends SQLiteOpenHelper {
	public static final int DATABASE_VERSION = 5;
	public static final String DATABASE_NAME = "database.db";
	/***********************FIELD***************************************/
	public static final String TABLE_FIELD = "field_info";
	public static final String KEY_ID = "_id";
	public static final String KEY_NAME = "name";
	public static final String KEY_AREA_1 = "area_1";
	public static final String KEY_CREATION_DATE = "craetion_date";
	public static final String KEY_SEASON = "season";
	public static final String KEY_GROUP = "field_group";
	public static final String KEY_FIELD_NUMBER = "field_number";
	public static final String KEY_CULTURE = "culture";
	public static final String KEY_RENT_OWN = "rent_own";
	public static final String KEY_COORDINATES = "coordinates";
	public static final String KEY_LAT = "lat";
	public static final String KEY_LNG = "lnt";
	/**************** POIS ********************/
	public static final String TABLE_POI = "poi";
	public static final String KEY_TITLE = "title";
	public static final String KEY_CATEGORY = "category";
	public static final String KEY_DESCRIPTION = "desription";    
	public static final String KEY_OBJECT_ID = "object_id";
	public static final String KEY_UNIQUE_NUMBER = "unique_number";
	public static final String KEY_DELETED = "deleted";
	
	public static final String KEY_DATE= "key_date";
	public static final String KEY_FIELD_ID= "key_field_id";
	
	public static final String TABLE_NABVIGATION_LINES = "navigation_lines";

	public static DB mInstance = null;

	public static DB getDB(Context ctx) {

		if (mInstance == null) {
			mInstance = new DB(ctx.getApplicationContext());
		}
		return mInstance;
	}

	public DB(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/**
	 * I�trina vis� duombaz�
	 */
	 public void dropDB(){
		 SQLiteDatabase db = getWritableDatabase();
		 db.delete(TABLE_FIELD, null, null);
		 db.delete(TABLE_POI, null, null);
		 db.delete(TABLE_NABVIGATION_LINES, null, null);

		 db.close();
		 //  ctx.deleteDatabase(DATABASE_NAME);
	 }

	 @Override
	 public void onCreate(SQLiteDatabase db) {
		 //Version 1
		 insertDefaultData();
		 createFieldsTable(db);
		 createPoiTable(db);
		 createNavigationLinesTable(db); 
	 }

	 @Override
	 public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		 switch(oldVersion) {   
		 } 
	 }

	 public void insertDefaultData() {
	 }

	 public void createFieldsTable(SQLiteDatabase db) {
		 String CREATE_FIELD_INFO_TABLE = "CREATE TABLE " + TABLE_FIELD + "("
				 + KEY_ID + " INTEGER PRIMARY KEY," 
				 + KEY_NAME + " VARCHAR,"
				 + KEY_AREA_1 + " REAL," 
				 + KEY_CREATION_DATE + " INTEGER," 
				 + KEY_GROUP + " INTEGER," 
				 + KEY_FIELD_NUMBER + " VARCHAR," 
				 + KEY_COORDINATES + " VARCHAR, " 
				 + KEY_DESCRIPTION + " VARCHAR,"
				 + KEY_UNIQUE_NUMBER +" INTEGER, "
				 + KEY_DELETED + " INTEGER DEFAULT 0)";
		 db.execSQL(CREATE_FIELD_INFO_TABLE);
	 }

	 public void createPoiTable(SQLiteDatabase db) {
		 String CREATE_POI_TABLE = "CREATE TABLE " + TABLE_POI + "(" 
				 + KEY_ID + " INTEGER PRIMARY KEY," 
				 + KEY_TITLE + " VARCHAR,"
				 + KEY_CATEGORY + " INTEGER," 
				 + KEY_DESCRIPTION + " VARCHAR,"
				 + KEY_LAT + " REAL," 
				 + KEY_LNG + " REAL)";
		 db.execSQL(CREATE_POI_TABLE);
	 }
	 
	 
	 public void createNavigationLinesTable(SQLiteDatabase db) 
	 {
		 String CREATE_NAVI_TABLE = "CREATE TABLE " + TABLE_NABVIGATION_LINES + "(" 
				 + KEY_ID + " INTEGER PRIMARY KEY," 
				 + KEY_TITLE + " VARCHAR,"
				 + KEY_FIELD_ID + " VARCHAR,"
				 + KEY_DESCRIPTION + " VARCHAR,"
				 + KEY_DATE + " INTEGER,"
				 + KEY_COORDINATES + " VARCHAR)";
		 db.execSQL(CREATE_NAVI_TABLE); 
	 }
	 
	 public void createSprayedPolysTable(SQLiteDatabase db)
	 {
		 
	 }
	 
	 

}
